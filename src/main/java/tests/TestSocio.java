package tests;

import models.transformers.Socio_Transformer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;


public class TestSocio {

    private static EntityManager manager;
    private static EntityManagerFactory emf;

    @SuppressWarnings("unchecked")
    public static void main(String[] args){

        Persistence.createEntityManagerFactory("persistencia");
        manager = emf.createEntityManager();

        List<Socio_Transformer> socio = (List<Socio_Transformer>) manager.createQuery("FROM socio").getResultList();
        System.out.println("En la base de datos hay " + socio.size() + " socios");

    }

}
