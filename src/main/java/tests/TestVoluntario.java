package tests;

import models.transformers.Equipo_Transformer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class TestVoluntario {
    //@PersistenceContext(unitName ="persistencia")
    private static EntityManager manager;
    private static EntityManagerFactory emf;

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {

        //Creamos el gestor de persistencia (EM)
        emf = Persistence.createEntityManagerFactory("persistencia");
        manager = emf.createEntityManager();

        List<Equipo_Transformer> equipo = (List<Equipo_Transformer>) manager.createQuery("FROM db_spm.voluntario").getResultList();
        System.out.println("En la base de datos hay " + equipo.size() + " voluntarios trabajando.");
    }
}
