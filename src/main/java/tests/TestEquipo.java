package tests;

import models.transformers.Equipo_Transformer;
import models.transformers.Voluntario_Transformer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.util.List;

public class TestEquipo {

    //@PersistenceContext(unitName ="persistencia")
    //private static EntityManager manager;
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistencia");

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {

        //Creamos el gestor de persistencia (EM)
        EntityManager manager = emf.createEntityManager();

        //PRUEBA AÑADIR NUEVO MIEMBRO DE EQUIPO DE TIPO VOLUNTARIO
        //Voluntario_Transformer miembro1 = new Voluntario_Transformer("Antonio", "Pérez", "Del Monto",  "625944513", "aperez", "passantonio", LocalDate.of(1991, 7, 21), LocalDate.of(2015, 7, 21), Equipo_Transformer.Rol.USUARIO, 1L, 40);
        Voluntario_Transformer miembro2 = new Voluntario_Transformer("Antonio", "Pérez", "Del Monto",  "625944513", "Julio", "passantonio", LocalDate.of(1991, 7, 21), LocalDate.of(2015, 7, 21), Equipo_Transformer.Rol.USUARIO, 1L, 40);


        manager.getTransaction().begin();
        manager.persist(miembro2);
        manager.getTransaction().commit();

        List<Equipo_Transformer> equipo = (List<Equipo_Transformer>) manager.createQuery("FROM equipo").getResultList();
        System.out.println("En la base de datos hay " + equipo.size() + " empleados trabajando.");

        for(Equipo_Transformer miembro: equipo) {
            System.out.println(miembro.toString());
        }

        //Cerramos el manager
        manager.close();
    }
}
