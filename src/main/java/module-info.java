module com.example.SPM_App {

    //EXPORTACIÓN DE LIBRERIAS GENERALES
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires jakarta.xml.bind;
    requires java.persistence;

    //EXPORTACIÓN DE CONTROLADORES
    exports controllers;
    opens controllers to javafx.fxml;

    exports  controllers.InterfacesPrincipales;
    opens  controllers.InterfacesPrincipales to javafx.fxml;

    exports  controllers.miembrosEquipo;
    opens  controllers.miembrosEquipo to javafx.fxml;

    exports  controllers.socios;
    opens  controllers.socios to javafx.fxml;


    //EXPORTACIÓN DE MODELOS
    exports models.transformers;
    opens models.transformers;

    exports tests;
    opens tests;
}