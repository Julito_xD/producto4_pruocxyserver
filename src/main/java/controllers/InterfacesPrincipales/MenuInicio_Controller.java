package controllers.InterfacesPrincipales;

import controllers.Controller_Manager;
import controllers.Controller_Father;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;


public class MenuInicio_Controller extends Controller_Father {

    // ------------------- DECLARACIÓN DE ATRIBUTOS ------------------- //

    @FXML
    private Label ID_MenuInicio;

    Controller_Manager controller_manager = new Controller_Manager();

    // ---------------------- GETTERS & SETTERS ----------------------- //


    // ----------------------- MÉTODOS DE CLASE ----------------------- //

    @FXML
    void btnSocios(ActionEvent event) {

        String title    = "MOSTRAR SOCIO";
        String viewPath = "/views/socios/MostrarSocio_View.fxml";
        controller_manager.abrirVista(title, viewPath, ID_MenuInicio);

    }

    @FXML
    void btnGestionEquipo(ActionEvent event) {

        String title    = "Miembros de equipo";
        String viewPath = "/views/miembrosEquipo/MiembroEquipoView.fxml";
        controller_manager.abrirVista(title, viewPath, ID_MenuInicio);

    }

    @FXML
    void btnAgenda(ActionEvent event) {

    }

    @FXML
    void btnContabilidad(ActionEvent event) {

    }

    @FXML
    void btnContactos(ActionEvent event) {

    }

    @FXML
    void btnListados(ActionEvent event) {

        String title    = "MENU LISTADOS";
        String viewPath = "/views/MenuListados_View.fxml";
        controller_manager.abrirVista(title, viewPath, ID_MenuInicio);

    }

    @FXML
    void btnSalir(ActionEvent event) {
        //TODO: ELIMINAR ESTÁ FUNCIÓN CUANDO SE IMPLEMENTE EL LOGIN.
        Stage myStage = (Stage) this.ID_MenuInicio.getScene().getWindow();
        myStage.close();

    }

    @Override
    public void closeWindows() {

    }
}

/* CÓDIGOS DESCARTADOS

//        try {
//
//            //Paso 0: Inicializamos un nuevo objeto de tipo renderizador
//            Stage stage = new Stage();
//
//            //Paso 1: Importamos la vista de los Inputs.
//            FXMLLoader view = new FXMLLoader(getClass().getResource("/views/socios/MostrarSocio_View.fxml"));
//            Scene scene = new Scene(view.load());
//
//            //Paso 2: Inicializamos el controlador y le entregamos la lista de personas antes de lanzar la vista
//            MostrarSocio_Controller controller = view.getController();
//
//            //Paso 3: Configuramos la vista
//            stage.setTitle("MENÚ DE SOCIOS");
//            stage.setScene(scene);
//
//            //Paso 4: Lanzamos la vista
//            System.out.println("\nVentana, mostrarSocio ejecutandose.");
//            stage.show();
//
//            //¿?¿?¿?
//            stage.setOnCloseRequest( e -> controller.closeWindows() );
//
//            //¿?¿?¿?
//            Stage myStage = (Stage) this.ID_MenuInicio.getScene().getWindow();
//            myStage.close();
//
//        } catch (IOException e) {
//
//            Alert alert = new Alert(Alert.AlertType.ERROR);
//            alert.setHeaderText(null);
//            alert.setTitle("Error");
//            alert.setContentText(e.getMessage());
//            alert.showAndWait();
//
//        }

 */