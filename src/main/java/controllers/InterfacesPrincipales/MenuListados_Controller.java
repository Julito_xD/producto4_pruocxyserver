package controllers.InterfacesPrincipales;

import controllers.Controller_Father;
import controllers.Controller_Manager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class MenuListados_Controller extends Controller_Father {

    // ------------------- DECLARACIÓN DE ATRIBUTOS ------------------- //

    @FXML
    private Label ID_MenuListados;

    Controller_Manager controller_manager = new Controller_Manager();

    // ---------------------- GETTERS & SETTERS ----------------------- //


    // ----------------------- MÉTODOS DE CLASE ----------------------- //

    @FXML
    void btnListadoEquipo(ActionEvent event) {

    }

    @FXML
    void btnListadoSocios(ActionEvent event) {

    }

    @FXML
    void btnSalir(ActionEvent event) {
        this.closeWindows();
    }

    @Override
    public void closeWindows() {

        String title    = "MENÚ PRINCIPAL";
        String viewPath = "/views/MenuInicio_View.fxml";
        controller_manager.abrirVista(title, viewPath, ID_MenuListados);

    }
}
