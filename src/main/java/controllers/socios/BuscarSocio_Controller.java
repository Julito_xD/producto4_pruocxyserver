package controllers.socios;

import controllers.Controller_Manager;
import controllers.Controller_Father;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class BuscarSocio_Controller extends Controller_Father {

    // ------------------- DECLARACIÓN DE ATRIBUTOS ------------------- //
    @FXML
    private Label ID_BuscarSocios;

    Controller_Manager controller_manager = new Controller_Manager();

    // ---------------------- GETTERS & SETTERS ----------------------- //

    // ----------------------- MÉTODOS DE CLASE ----------------------- //

    @FXML
    void btnGrabar(ActionEvent event) {

    }

    @FXML
    void btnCancelar(ActionEvent event) {

    }

    @FXML
    void btnNuevo(ActionEvent event) {
        //TODO: ANULARLO
    }

    @FXML
    void btnBorrar(ActionEvent event) {

    }

    @FXML
    void btnBusqueda(ActionEvent event) {

    }

    @FXML
    void btnSalir(ActionEvent event) {
        this.closeWindows();
    }

    @Override
    public void closeWindows() {

        String title    = "MOSTRAR SOCIO";
        String viewPath = "/views/socios/MostrarSocio_View.fxml";
        controller_manager.cerrarVista(title, viewPath, ID_BuscarSocios);

    }
}
