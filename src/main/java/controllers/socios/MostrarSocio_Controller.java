package controllers.socios;

import controllers.Controller_Manager;
import controllers.Controller_Father;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class MostrarSocio_Controller extends Controller_Father {

    // ------------------- DECLARACIÓN DE ATRIBUTOS ------------------- //
    @FXML
    private Label ID_MostrarSocios;

    Controller_Manager controller_manager = new Controller_Manager();

    // ---------------------- GETTERS & SETTERS ----------------------- //


    // ----------------------- MÉTODOS DE CLASE ----------------------- //
    @FXML
    void btnGrabar(ActionEvent event) {
    }

    @FXML
    void btnCancelar(ActionEvent event) {
    }

    @FXML
    void btnNuevo(ActionEvent event) {

        String title    = "AGREGAR NUEVO SOCIO";
        String viewPath = "/views/socios/AgregarSocio_View.fxml";
        controller_manager.abrirVista(title, viewPath, ID_MostrarSocios);

    }

    @FXML
    void btnBorrar(ActionEvent event) {
    }

    @FXML
    void btnBusqueda(ActionEvent event) {

        String title    = "BUSCAR SOCIO";
        String viewPath = "/views/socios/BuscarSocio_View.fxml";
        controller_manager.abrirVista(title, viewPath, ID_MostrarSocios);

    }

    @FXML
    void btnSalir(ActionEvent event) {
        this.closeWindows();
    }

    public void closeWindows() {

        String title    = "MENÚ PRINCIPAL";
        String viewPath = "/views/MenuInicio_View.fxml";
        controller_manager.cerrarVista(title, viewPath, ID_MostrarSocios);

    }

}

/* CÓDIGOS DESCARTADOS

//        try {
//
//            //Paso 0: Inicializamos un nuevo objeto de tipo renderizador
//            Stage stage = new Stage();
//
//            //Paso 1: Importamos la nueva vista
//            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/MenuInicio_View.fxml"));
//            Scene scene = new Scene(fxmlLoader.load());
//
//            //Paso 3: Configuramos la vista
//            stage.setTitle("ADMIN");
//            stage.setScene(scene);
//
//            //Paso 4: Lanzamos la vista
//            System.out.println("Ventana, menuPrincipal ejecutandose.");
//            stage.show();
//
//            //Cerramos la vista
//            Stage myStage = (Stage) this.ID_MostrarSocios.getScene().getWindow();
//            myStage.close();
//
//        } catch (IOException e) {
//
//            Alert alert = new Alert(Alert.AlertType.ERROR);
//            alert.setHeaderText(null);
//            alert.setTitle("Error");
//            alert.setContentText(e.getMessage());
//            alert.showAndWait();
//
//        }

 */