package controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller_Manager {

    /**
     * Opens a new VIEW
     * @param title Put here the title of the new View
     * @param viewPath Put here the Path of the new View
     * @param idReturnView Put here the ID from the view, when you want comeback when you close the new View.
     */
    public void abrirVista(String title, String viewPath, Label idReturnView){

        try {

            //Paso 0: Inicializamos un nuevo objeto de tipo renderizador
            Stage stage = new Stage();

            //Paso 1: Importamos la vista de los Inputs.
            FXMLLoader view = new FXMLLoader(getClass().getResource(viewPath));
            Scene scene = new Scene(view.load());

            //Paso 2: Inicializamos el controlador y le entregamos la lista de personas antes de lanzar la vista
            Controller_Father controller = view.getController();

            //Paso 3: Configuramos la vista
            stage.setTitle(title);
            stage.setScene(scene);

            //Paso 4: Lanzamos la vista
            System.out.println("\nVentana, " +title+ " ejecutandose.");
            stage.show();

            //¿?¿?¿?
            stage.setOnCloseRequest( e -> controller.closeWindows() );

            //¿?¿?¿?
            Stage myStage = (Stage) idReturnView.getScene().getWindow();
            myStage.close();

        } catch (IOException e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText(e.getMessage());
            alert.showAndWait();

        }
    }

    /**
     * Closes the current view
     * @param title Put here the title of the View, you want comeback.
     * @param viewPath Put here the Path of the View, you want comeback.
     * @param idReturnView Put here the ID from the current View, to close it when you leave.
     */
    public void cerrarVista(String title, String viewPath, Label idReturnView){

        try {

            //Paso 0: Inicializamos un nuevo objeto de tipo renderizador
            Stage stage = new Stage();

            //Paso 1: Importamos la nueva vista
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(viewPath));
            Scene scene = new Scene(fxmlLoader.load());

            //Paso 3: Configuramos la vista
            stage.setTitle(title);
            stage.setScene(scene);

            //Paso 4: Lanzamos la vista
            System.out.println("Ventana, " +title+ " ejecutandose.");
            stage.show();

            //Cerramos la vista
            Stage myStage = (Stage) idReturnView.getScene().getWindow();
            myStage.close();

        } catch (IOException e) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText(e.getMessage());
            alert.showAndWait();

        }

    }

}
