package controllers.miembrosEquipo;

import controllers.Controller_Father;
import controllers.Controller_Manager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class AgregarMiembroEquipo_Controller extends Controller_Father {


    @FXML
    private Label IDMiembroEquipo;

    Controller_Manager controller_manager = new Controller_Manager();

    @FXML
    private Button grabar;

    @FXML
    private TextField id1;

    @FXML
    private TextField id2;

    @FXML
    private TextField id3;

    @FXML
    void grabar(ActionEvent event) {

    }

    @FXML
    void btnSalir(ActionEvent event) {
        this.closeWindows();
    }

    @Override
    public void closeWindows() {

        String title    = "Miembro de equipo";
        String viewPath = "/views/miembrosEquipo/MiembroEquipoView.fxml";
        controller_manager.cerrarVista(title, viewPath, IDMiembroEquipo);
    }
}
