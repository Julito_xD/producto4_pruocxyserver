package controllers;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main_Controller extends Application {

    @Override
    public void start(Stage stage) throws IOException {

//        TODO: NO ELIMINAR!!!
//        FXMLLoader fxmlLoader = new FXMLLoader(Main_Controller.class.getResource("/views/Login_View.fxml"));
//        Scene scene = new Scene(fxmlLoader.load());
//        stage.setTitle("USER AUTHENTICATION");
//        stage.setScene(scene);
//        stage.show();

        FXMLLoader fxmlLoader = new FXMLLoader(Main_Controller.class.getResource("/views/MenuInicio_View.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("MENÚ PRINCIPAL");
        stage.setScene(scene);
        System.out.println("Bienvenido a la APP de PrUOCxy_Server");
        stage.show();

    }

    public static void main(String[] args) {
        launch();
    }

}


