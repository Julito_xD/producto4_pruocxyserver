package models.entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Socio {

    //ATRIBUTOS
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "IdSocio")
    private Long idSocio;
    @Basic
    @Column(name = "Nombre")
    private String nombre;
    @Basic
    @Column(name = "Apellido1")
    private String apellido1;
    @Basic
    @Column(name = "Apellido2")
    private String apellido2;
    @Basic
    @Column(name = "NIF")
    private String nif;
    @Basic
    @Column(name = "Dirección")
    private String dirección;
    @Basic
    @Column(name = "FechaNacimiento")
    private Date fechaNacimiento;
    @Basic
    @Column(name = "Movil")
    private String movil;
    @Basic
    @Column(name = "Correo")
    private String correo;
    @Basic
    @Column(name = "Sexo")
    private Object sexo;
    @Basic
    @Column(name = "tipoCuota")
    private Object tipoCuota;
    @Basic
    @Column(name = "FechaAlta")
    private Date fechaAlta;
    @Basic
    @Column(name = "FechaBaja")
    private Date fechaBaja;
    @Basic
    @Column(name = "NroCuenta")
    private String nroCuenta;
    @Basic
    @Column(name = "idONG")
    private Long idOng;

    //CONTRUCTOR
    public Socio() {
    }

    //GETTERS & SETTERS
    public Long getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(Long idSocio) {
        this.idSocio = idSocio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getDirección() {
        return dirección;
    }

    public void setDirección(String dirección) {
        this.dirección = dirección;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Object getSexo() {
        return sexo;
    }

    public void setSexo(Object sexo) {
        this.sexo = sexo;
    }

    public Object getTipoCuota() {
        return tipoCuota;
    }

    public void setTipoCuota(Object tipoCuota) {
        this.tipoCuota = tipoCuota;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public String getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public Long getIdOng() {
        return idOng;
    }

    public void setIdOng(Long idOng) {
        this.idOng = idOng;
    }

    //MÉTODOS DE CLASE
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Socio socio = (Socio) o;
        return Objects.equals(idSocio, socio.idSocio) && Objects.equals(nombre, socio.nombre) && Objects.equals(apellido1, socio.apellido1) && Objects.equals(apellido2, socio.apellido2) && Objects.equals(nif, socio.nif) && Objects.equals(dirección, socio.dirección) && Objects.equals(fechaNacimiento, socio.fechaNacimiento) && Objects.equals(movil, socio.movil) && Objects.equals(correo, socio.correo) && Objects.equals(sexo, socio.sexo) && Objects.equals(tipoCuota, socio.tipoCuota) && Objects.equals(fechaAlta, socio.fechaAlta) && Objects.equals(fechaBaja, socio.fechaBaja) && Objects.equals(nroCuenta, socio.nroCuenta) && Objects.equals(idOng, socio.idOng);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idSocio, nombre, apellido1, apellido2, nif, dirección, fechaNacimiento, movil, correo, sexo, tipoCuota, fechaAlta, fechaBaja, nroCuenta, idOng);
    }

    @Override
    public String toString() {
        return "Socio{" +
                "idSocio=" + idSocio +
                ", nombre='" + nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", nif='" + nif + '\'' +
                ", dirección='" + dirección + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", movil='" + movil + '\'' +
                ", correo='" + correo + '\'' +
                ", sexo=" + sexo +
                ", tipoCuota=" + tipoCuota +
                ", fechaAlta=" + fechaAlta +
                ", fechaBaja=" + fechaBaja +
                ", nroCuenta='" + nroCuenta + '\'' +
                ", idOng=" + idOng +
                '}';
    }
}
