package models;

import models.models.SQLconnection;
import dao.DAO_Factory;
import jakarta.xml.bind.JAXBException;
import models.transformers.Delegacion_Transformer;
import models.transformers.SedeCentral_Transformer;

import java.sql.Connection;
import java.sql.SQLException;


public class MainSQL {
    public static void main(String[] args) throws SQLException, JAXBException {

        Connection conn = null;

        try {
            new SQLconnection();
            conn = SQLconnection.getConexion();

            // ---------------------------------- INSTANCIE AQUÍ SUS CLASES ---------------------------------- //
            DAO_Factory FactoriaMySQL = DAO_Factory.getFactoryIMPL(DAO_Factory.MYSQL);
            FactoriaMySQL.setConnection(conn);

            // ----------------------- JULITO -------------------------//
            // -------------------------------------------------------//
            // --- INSTANCIACIÓN DE LOS MÉTODOS CRUD DE SEDECENTRAL Y DELEGACIÓN ---//

            //[Instantiating objects]
            SedeCentral_Transformer s1 = new SedeCentral_Transformer("SedeCentral", "España");
            Delegacion_Transformer d1 = new Delegacion_Transformer("delegaciónLaurita", "Palma");
            Delegacion_Transformer d2 = new Delegacion_Transformer("delegaciónJordiño", "Sarria Pijo");
            Delegacion_Transformer d3 = new Delegacion_Transformer("delegaciónSofia", "Sant Cugat");
            Delegacion_Transformer d4 = new Delegacion_Transformer("delegaciónShaila", "Martorell");
            Delegacion_Transformer d5 = new Delegacion_Transformer("delegaciónJulito", "The best place on the World");
            Delegacion_Transformer d6 = new Delegacion_Transformer("delegaciónPrueba", "Pobre pruebita");

            //Testing INSERT [TESTED OK]
            FactoriaMySQL.getSedeCentralIMPL().insert(s1);
            FactoriaMySQL.getDelegacionIMPL().insert(d1);
            FactoriaMySQL.getDelegacionIMPL().insert(d2);
            FactoriaMySQL.getDelegacionIMPL().insert(d3);
            FactoriaMySQL.getDelegacionIMPL().insert(d4);
            FactoriaMySQL.getDelegacionIMPL().insert(d5);
            FactoriaMySQL.getDelegacionIMPL().insert(d6);

            //Testing UDATE [TESTED OK]
            //Delegacion upd = new Delegacion(8,"delegaciónPruebita", "Pronto se eliminará");
            //FactoriaMySQL.getDelegacionIMPL().update(upd);
            //FactoriaMySQL.getSedeCentralIMPL().update_nomSede(2L, "SedeEspaña");
            //FactoriaMySQL.getSedeCentralIMPL().update_ubicación(2L, "Mi casa");

            //Testing DELETE [TESTED OK]
            //FactoriaMySQL.getDelegacionIMPL().deleID(8L);

            //Testing GETONE [TESTED OK]
            //System.out.println(FactoriaMySQL.getDelegacionIMPL().getOne(5L));

            //Testing GETALL [TESTED OK]
            //FactoriaMySQL.getSedeCentralIMPL().getAll();
            //FactoriaMySQL.getDelegacionIMPL().getAll();

            // ------------------------SHAILA-------------------------//
            // -------------------------------------------------------//
            // --- INSTANCIACIÓN DE LOS MÉTODOS CRUD DE COLABORADOR---//

            //Testing INSERT [TESTED OK]
            /*
            Colaborador colaborador1 = new Colaborador("Shaila", "Gibert", "Marco",
                    "618985046", "sgibertma", "Family33!",
                    LocalDate.parse("2021-12-05"), LocalDate.parse("2021-12-12"), ADMINISTRADOR,
                    1L, "Colaboracion administrativa");
            Colaborador colaborador2 = new Colaborador("Bogdan", "Bogdanov", "",
                    "977505021", "bbogdanov", "Aventura33*",
                    LocalDate.parse("2021-12-02"), LocalDate.parse("2022-12-31"), USUARIO, 1L, "Colaborador puntual");
            Colaborador colaborador3 = new Colaborador("Paquita", "Marco", "Bono","615268955", "pmarcobono", "Pinxo63",
                    LocalDate.parse("2022-02-02"), LocalDate.parse("2022-12-12"), USUARIO, 1L, "Colaboracion maternal");

            //FactoriaMySQL.getColaboradorIMPL().insert(colaborador1);
            //FactoriaMySQL.getColaboradorIMPL().insert(colaborador2);
            //FactoriaMySQL.getColaboradorIMPL().insert(colaborador3);
             */

            //Testing UPDATE [TESTED OK]
            /*
            Colaborador colaborador1v2 = new Colaborador("Josep", "Gibert", "Saco", 3L, "615268954","josepmgibert", "1234",
                    LocalDate.parse("2021-12-05"), LocalDate.parse("2022-12-31"), USUARIO, 1L, "Colaboracion familiar de prueba");
            //FactoriaMySQL.getColaboradorIMPL().update(colaborador1v2);
            */

            //Testing DELETE [TESTED OK]
            //FactoriaMySQL.getColaboradorIMPL().delete(colaborador1v2);

            //Testing GETONE [TESTED OK]
            //FactoriaMySQL.getColaboradorIMPL().getOne(2L);

            //Testing GETALL [TESTED OK]
            //FactoriaMySQL.getColaboradorIMPL().getAll();


            // ----------------------- JORDI -------------------------//
            // -------------------------------------------------------//
            // --- INSTANCIACIÓN DE LOS MÉTODOS CRUD DE SOCIO ---//

            //Socio socio1 = new Socio("Julito","Don","Perignon","5623717H","210004042323422234",LocalDate.parse("1995-07-21"),LocalDate.parse("1995-07-22"),"MENSUAL",1L);
            /*
            Socio socio2 = new Socio("Laurita","Amorcito","Platonico","45678901K","210004042323422289", LocalDate.parse("2020-03-20"),LocalDate.parse("2020-03-20"),"ANUAL",1L);
            Socio socio3 = new Socio("Shailita","Asiatica","Killer","12345678P","210004042323422231",LocalDate.parse("2020-03-23"),LocalDate.parse("2020-03-23"),"TRIMESTRAL",1L);
            Socio socio4 = new Socio("Sofia","Venezolana","Arriba","34567890L","210004042323422237",LocalDate.parse("2020-05-19"),LocalDate.parse("2020-05-19"), "MENSUAL",1L);

            FactoriaMySQL.getSocioIMPL().insert(socio1);
            FactoriaMySQL.getSocioIMPL().insert(socio2);
            FactoriaMySQL.getSocioIMPL().insert(socio3);
            FactoriaMySQL.getSocioIMPL().insert(socio4);
            */

            //TESTEO DEL CRUD DELETE:
            /*
            Socio socio1 = new Socio(1L,"Julito","Don","Perignon","5623717H","210004042323422234",LocalDate.parse("1995-07-21"),LocalDate.parse("1995-07-22"),"MENSUAL",1L);
            FactoriaMySQL.getSocioIMPL().delete(socio1);
            */

            //TESTEO DEL CRUD GETONE:
            //FactoriaMySQL.getSocioIMPL().getOne(2L);

            //TESTEO DEL CRUD GETALL:
            //FactoriaMySQL.getSocioIMPL().getAll();

            // --- INSTANCIACIÓN DE LOS MÉTODOS CRUD DE ¿?¿? ---//
            //[Instantiating objects]


            //Testing INSERT [TESTED NULL]

            // ----------------------- LAURA -------------------------//
            // -------------------------------------------------------//
            // --- INSTANCIACIÓN DE LOS MÉTODOS CRUD DE INGRESOPUBLICO E INGRESOPRIVADO ---//

            //--- TESTEO DEL CRUD INSERT:
            //[Instantiating objects]
            /*
            IngresoPublico ingPub1 = new IngresoPublico(5000, "AP_PUNTUAL", LocalDate.parse("2018-10-30"),
                            "Aportaciones S.L.", "6554867321X", 1L, "ADMIN_ESTATAL");
            IngresoPublico ingPub2 = new IngresoPublico(8000, "AP_PUNTUAL", LocalDate.parse("2020-02-25"),
                            "Ayuntamiento de Madrid", "6554867321X", 1L, "ADMIN_ESTATAL");
            IngresoPublico ingPub3 = new IngresoPublico(4500, "AP_PUNTUAL", LocalDate.parse("2020-07-20"),
                            "Ayuntamiento de Barcelona", "6947672551X", 1L, "ADMIN_ESTATAL");

            IngresoPrivado ingPriv1 = new IngresoPrivado(5000, "AP_PUNTUAL", LocalDate.parse("2018-10-30"),
                            "Aportaciones S.L.", "6554867321X", 1L, "INGRESOEXTRA", 2);
            IngresoPrivado ingPriv2 = new IngresoPrivado(8000, "AP_PUNTUAL", LocalDate.parse("2020-02-25"),
                            "Ayuntamiento de Madrid", "6554867321X", 1L, "INGRESOEXTRA", 2);
            IngresoPrivado ingPriv3 = new IngresoPrivado(7500, "AP_PUNTUAL", LocalDate.parse("2020-04-12"),
                                        "Ayuntamiento de Valencia", "8259033621X", 1L, "INGRESOEXTRA", 2);
            */

            //REVISAR LINEA 147 de MYSQLIngresoPrivado.java

            //Testing INSERT [TESTED OK]
            //FactoriaMySQL.getIngresoPublicoIMPL().insert(ingPub1);
            //FactoriaMySQL.getIngresoPublicoIMPL().insert(ingPub2);
            //FactoriaMySQL.getIngresoPublicoIMPL().insert(ingPub3);
            //FactoriaMySQL.getIngresoPrivadoIMPL().insert(ingPriv1);
            //FactoriaMySQL.getIngresoPrivadoIMPL().insert(ingPriv2);
            //FactoriaMySQL.getIngresoPrivadoIMPL().insert(ingPriv3);

            //---TESTEO DEL CRUD UPDATE:
            //[Instantiating objects]
            //IngresoPublico ingPub1 = new IngresoPublico(10, 11500, "AP_RECURRENTE", LocalDate.parse("2018-10-30"),
            //                "Inditex", "6554867321X", 1L, "ADMIN_AUTONOMICA");

            //Testing UDATE [TESTED NULL]
            //FactoriaMySQL.getIngresoPublicoIMPL().update(ingPub1);

            //Testing DELETE [TESTED MIDDLE GOOD]
            //FactoriaMySQL.getIngresoPublicoIMPL().deleID(3L);
            //FactoriaMySQL.getIngresoPrivadoIMPL().deleID(5L);

            //Testing GETONE [TESTED OK]
            //System.out.println(FactoriaMySQL.getIngresoPublicoIMPL().getOne(2L));

            //Testing GETALL [TESTED OK]
            //FactoriaMySQL.getIngresoPublicoIMPL().getAll();
            //FactoriaMySQL.getIngresoPrivadoIMPL().getAll();

            // ----------------------- SOFIA -------------------------//
            // -------------------------------------------------------//
            // --- INSTANCIACIÓN DE LOS MÉTODOS CRUD DE LINEAACCION Y SUBLINEA_ACCION ---//

            //[Instantiating objects]
            //LineaAccion l1 = new LineaAccion(1L, Fortalecimiento_Institucional);
            //LineaAccion l2 = new LineaAccion(1L, Cooperación_Desarrollo);
            //SublineaAccion sa1 = new SublineaAccion(1L, "Porqué quieren sentirse bien");
            //SublineaAccion sa2 = new SublineaAccion(1L, "Porque si");
            //SublineaAccion sa3 = new SublineaAccion(2L, "Por la paz");

            //Testing INSERT [TESTED OK]
            //FactoriaMySQL.getLineaAccionIMPL().insert(l1);
            //FactoriaMySQL.getLineaAccionIMPL().insert(l2);
            //FactoriaMySQL.getSublineaAccionIMPL().insert(sa1);
            //FactoriaMySQL.getSublineaAccionIMPL().insert(sa2);
            //FactoriaMySQL.getSublineaAccionIMPL().insert(sa3);

            //Testing UDATE [TESTED OK]
            //LineaAccion l1 = new LineaAccion(1L, Educación_Para_Desarrollo);
            //FactoriaMySQL.getLineaAccionIMPL().update(l1);

            //SublineaAccion upd = new SublineaAccion(3,"prueba", "prueba2");
            //FactoriaMySQL.getSublineaAccionIMPL().update(upd);
            //FactoriaMySQL.getSublineaAccionIMPL().update_idLinea(1L, "Cooperación_Desarrollo");
            //FactoriaMySQL.getSublineaAccionIMPL().update_Concepto(1L, "Porque quieren ser felices");

            //Testing DELETE [TESTED OK]
            //FactoriaMySQL.getLineaAccionIMPL().delete(l1);


            //Testing GETONE [TESTED NULL]
            //System.out.println(FactoriaMySQL.getLineaAccionIMPL().getOne(2L));


            //Testing GETALL [TESTED NULL]
            //FactoriaMySQL.getLineaAccionIMPL().getAll();

        } finally {
            if (conn != null){
                conn.close();
            }
        }
    }
}
