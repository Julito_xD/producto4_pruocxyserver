package models.models;

import models.transformers.IngresoPrivado_Transformer;
import dao.IngresoPrivadoDAO;
import jakarta.xml.bind.JAXBException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementación del DAO para la clase IngresoPrivado para MySQL
 * @author laurazp
 */

public class MySQLingresoPrivadoIMPL implements IngresoPrivadoDAO {
    // ---------------------------------- ENTORNO ---------------------------------- //
    // QUERIES
    final String INSERT = "INSERT INTO db_spm.INGRESO (cantidad, tipoAportacion, fechaIngreso, nombreAportador, idAportador, idONG) " +
            "VALUES (?, ?, ?, ?, ?, ?);";
    final String INHIJO = "INSERT INTO db_spm.INGRESO_PRIVADO (idIngreso, origenAportacion, idSocio) VALUES (?, ?, ?);";
    final String UPDATE = "UPDATE db_spm.INGRESO SET cantidad = ?, tipoAportacion = ?, fechaIngreso = ?, nombreAportador = ?, " +
            "idAportador = ?, idONG = ?, origenAportacion = ?, idSocio = ? WHERE idIngreso = ?;";
    final String DELETE = "DELETE FROM db_spm.INGRESO WHERE idIngreso = ?;";
    final String GETONE = "CALL GETONE_INGRESO_PRIVADO(?);";
    final String GETALL = "CALL GETALL_INGRESO_PRIVADO();";
    //-------- Métodos CRUD UPDATE opcionales de la clase IngresoPrivado que modifican sólo un parámetro
    final String UPD_CANTIDAD = "UPDATE db_spm.INGRESO SET cantidad = ? WHERE idIngreso = ?;";
    final String UPD_NOMAPORT = "UPDATE db_spm.INGRESO SET nombreAportador = ? WHERE idIngreso = ?;";


    // DECLARACIÓN DE ATRIBUTOS GLOBALES
    private Connection conn;
    private PreparedStatement stat;
    private ResultSet rs;

    // MÉTODO CONSTRUCTOR
    public MySQLingresoPrivadoIMPL(Connection conn){
        this.conn = conn;
    }

    // -------------------------------- MÉTODOS CRUD -------------------------------- //
    @Override
    public void insert(IngresoPrivado_Transformer ingresoPrivadoTransformer) throws JAXBException, SQLException {
        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

        //Se ejecuta la QUERY para la entidad PADRE dando valores a sus campos.
        stat.setFloat(1, ingresoPrivadoTransformer.getCantidad ());
        stat.setObject(2, ingresoPrivadoTransformer.getTipoAportacion());
        stat.setDate(3, new Date(LocalDateToLong.getLong(ingresoPrivadoTransformer.getFechaIngreso())));
        stat.setString(4, ingresoPrivadoTransformer.getNombreAportador());
        stat.setString(5, ingresoPrivadoTransformer.getIdAportador());
        stat.setLong(6, ingresoPrivadoTransformer.getIdOng());
        SQLManager.executeUpdate(stat);
        //Se asigna la Key al objeto autoincrementandola desde la BBDD.
        rs = SQLManager.generateKey(stat);
        ingresoPrivadoTransformer.setIdIngreso(rs.getLong(1));
        //Se cierra la conexion del Statement y la conexión del ResultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Se ejecuta la QUERY para la entidad HIJO dando valores a sus campos.
        stat = conn.prepareStatement(INHIJO);
        stat.setLong(1, ingresoPrivadoTransformer.getIdIngreso());
        stat.setObject(2, ingresoPrivadoTransformer.getOrigenAportacion());
        stat.setLong(3, ingresoPrivadoTransformer.getIdSocio());
        SQLManager.executeUpdate(stat);
        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoINSERT(ingresoPrivadoTransformer);
        SQLManager.statClose(stat);
    }

    @Override
    public void update(IngresoPrivado_Transformer ingresoPrivadoTransformer) throws JAXBException, SQLException {
        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(UPDATE);

        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setFloat(1, ingresoPrivadoTransformer.getCantidad ());
        stat.setObject(2, ingresoPrivadoTransformer.getTipoAportacion());
        stat.setDate(3, new Date(LocalDateToLong.getLong(ingresoPrivadoTransformer.getFechaIngreso())));
        stat.setString(4, ingresoPrivadoTransformer.getNombreAportador());
        stat.setString(5, ingresoPrivadoTransformer.getIdAportador());
        stat.setLong(6, ingresoPrivadoTransformer.getIdOng());
        stat.setObject(7, ingresoPrivadoTransformer.getOrigenAportacion());
        stat.setObject(8, ingresoPrivadoTransformer.getIdSocio());
        stat.setLong(9, ingresoPrivadoTransformer.getIdIngreso());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoUPDATE(ingresoPrivadoTransformer);
        SQLManager.statClose(stat);
    }

    @Override
    public void delete(IngresoPrivado_Transformer ingresoPrivadoTransformer) throws JAXBException, SQLException {
        String ingresoPrivadoString = ingresoPrivadoTransformer.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos el valor de la ID de la Entidad que queremos eliminar.
        stat.setLong(1, ingresoPrivadoTransformer.getIdIngreso());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(ingresoPrivadoString);
        SQLManager.statClose(stat);
    }

    @Override
    public void deleID(Long id) throws SQLException {
        //Almacenamos el Objeto que se va a eliminar en un STRING para imprimir luego su resultado por consola.
        IngresoPrivado_Transformer ingPriv = this.getOne(id); //Usamos el método GETONE para acceder al objeto a partir de su ID
        String ingresoPrivadoString = ingPriv.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos el valor de la ID de la Entidad que queremos eliminar.
        stat.setLong(1, id);

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(ingresoPrivadoString);
        SQLManager.statClose(stat);
    }

    private IngresoPrivado_Transformer convertirREGISTROaOBJETO(ResultSet rs) throws SQLException {
        //Se transforman los campos de la tabla en atributos del objeto
        float cantidad  = rs.getFloat("cantidad");
        String tipoAportacion  = rs.getString("tipoAportacion");
        LocalDate fechaIngreso  = LocalDate.parse(rs.getString("fechaIngreso"));
        String nombreAportador  = rs.getString("nombreAportador");
        String idAportador  = rs.getString("idAportador");
        Long idOng = rs.getLong("idOng");
        String origenAportacion  = rs.getString("origenAportacion");
        int idSocio = rs.getInt("idSocio");
        //Se instancia el objeto
        IngresoPrivado_Transformer ingresoPrivadoTransformer = new IngresoPrivado_Transformer(cantidad, tipoAportacion, fechaIngreso, nombreAportador,
                idAportador, idOng, origenAportacion, idSocio);
        ingresoPrivadoTransformer.setIdIngreso(rs.getLong("idIngreso"));
        return ingresoPrivadoTransformer;
    }

    @Override
    public IngresoPrivado_Transformer getOne(Long id) throws SQLException {
        //Se crea el nuevo OBJETO.
        IngresoPrivado_Transformer ingresoPrivadoTransformer = null;

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETONE);
        stat.setLong(1, id);
        rs = stat.executeQuery();

        //Se agrega el REGISTRO extraido desde la BBDD al nuevo OBJETO
        if (rs.next()) ingresoPrivadoTransformer = convertirREGISTROaOBJETO(rs);
        else throw new SQLException("No se ha encontrado ese registro");

        //Se cierra la conexión del Statement y la conexión del ResultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Devolución de los resultados:
        //System.out.println(SQLManager.resultadoGETONE(ingresoPrivado));
        return ingresoPrivadoTransformer;
    }

    @Override
    public List<IngresoPrivado_Transformer> getAll() throws SQLException {
        //Se crea el nuevo OBJETO.
        List<IngresoPrivado_Transformer> ingresosPrivados = new ArrayList<>();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETALL);
        rs = stat.executeQuery();

        //Se agregan todos los REGISTROS extraidos desde la BBDD a la nueva LISTA, convirtiéndolos a objetos.
        while (rs.next()){
            ingresosPrivados.add(convertirREGISTROaOBJETO(rs));
        }

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Devolución de los resultados:
        SQLManager.imprimirResultadoGETALL(ingresosPrivados);
        return ingresosPrivados;
    }
}
