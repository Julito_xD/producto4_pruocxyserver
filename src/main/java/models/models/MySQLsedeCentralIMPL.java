package models.models;

import models.transformers.SedeCentral_Transformer;
import dao.SedeCentralDAO;
import jakarta.xml.bind.JAXBException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLsedeCentralIMPL implements SedeCentralDAO {
    // ---------------------------------- ENTORNO ---------------------------------- //
    // QUERYES
    final String INSERT = "INSERT INTO bbdd_ong (nombreSede, ubicacion) VALUES (?, ?);";
    final String INHIJO = "INSERT INTO bbdd_sedecentral(idOng) VALUES (?);";
    final String UPDATE = "UPDATE bbdd_ong SET nombreSede = ?, ubicacion = ? WHERE idOng = ?;";
    final String UPD_NS = "UPDATE bbdd_ong SET nombreSede = ?   WHERE idOng = ?;";
    final String UP_UBI = "UPDATE bbdd_ong SET ubicacion = ?    WHERE idOng = ?;";
    final String DELETE = "DELETE FROM bbdd_ong WHERE idOng = ?";
    final String GETONE = "CALL GETONE_BBDD_SEDECENTRAL(?)";
    final String GETALL = "CALL GETALL_BBDD_SEDECENTRAL()";


    // DECLARACIÓN DE ATRIBUTOS GLOBALES
    private Connection conn;
    private PreparedStatement stat;
    private ResultSet rs;

    // MÉTODO CONSTRUCTOR
    public MySQLsedeCentralIMPL (Connection conn){
        this.conn = conn;
    }

    // -------------------------------- MÉTODOS CRUD -------------------------------- //
    @Override
    public void insert(SedeCentral_Transformer sedeCentralTransformer) throws JAXBException, SQLException {
        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

        //Se le da valores a los (?) de la QUERY
        stat.setString(1, sedeCentralTransformer.getNombreSede());
        stat.setString(2, sedeCentralTransformer.getUbicacion());
        SQLManager.executeUpdate(stat);
        //Se asigna la Key al objeto autoincrementandola desde la BBDD.
        rs = SQLManager.generateKey(stat);
        sedeCentralTransformer.setIdONG(rs.getLong(1));
        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Se ejecuta la QUERY para la entidad HIJO dando valores a sus campos.
        stat = conn.prepareStatement(INHIJO);
        stat.setLong(1, sedeCentralTransformer.getIdONG());
        SQLManager.executeUpdate(stat);
        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoINSERT(sedeCentralTransformer);
        SQLManager.statClose(stat);
    }

    @Override
    public void update(SedeCentral_Transformer sedeCentralTransformer) throws JAXBException, SQLException {
        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(UPDATE);

        //Se le da valores a los (?) de la QUERY
        stat.setString(1, sedeCentralTransformer.getNombreSede());
        stat.setString(2, sedeCentralTransformer.getUbicacion());
        stat.setLong(3, sedeCentralTransformer.getIdONG());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoUPDATE(sedeCentralTransformer);
        SQLManager.statClose(stat);
    }

    // -------------- EXPERIMENTALES --------------//
    public void update_nomSede(Long id, String nuevoNomSede) throws SQLException {
        //Se imprime por consola el Objeto que se va a modificar.
        SedeCentral_Transformer sc = this.getOne(id);
        SQLManager.imprimirResultadoGETONE(sc);

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(UPD_NS);

        //Se le da valores a los (?) de la QUERY
        stat.setString(1, nuevoNomSede);
        stat.setLong(2, id);

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        sc = this.getOne(id);
        SQLManager.imprimirResultadoUPDATE(sc);
        SQLManager.statClose(stat);
    }

    public void update_ubicación(Long id, String nuevaUbicacion) throws SQLException {
        //Se imprime por consola el Objeto que se va a modificar.
        SedeCentral_Transformer sc = this.getOne(id);
        SQLManager.imprimirResultadoGETONE(sc);

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(UP_UBI);

        //Se le da valores a los (?) de la QUERY
        stat.setString(1, nuevaUbicacion);
        stat.setLong(2, id);

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        sc = this.getOne(id);
        SQLManager.imprimirResultadoUPDATE(sc);
        SQLManager.statClose(stat);
    }
    // -------------- FIN EXPERIMENTALES --------------//

    @Override
    public void delete(SedeCentral_Transformer sedeCentralTransformer) throws JAXBException, SQLException {
        String sedeCentralString = sedeCentralTransformer.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Se le da valores a los (?) de la QUERY
        stat.setLong(1, sedeCentralTransformer.getIdONG());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(sedeCentralString);
        SQLManager.statClose(stat);
    }

    @Override
    public void deleID(Long id) throws SQLException {
        //Se guarda el registro en un STRING para poder imprimirlo luego de ser eliminado.
        SedeCentral_Transformer sc = this.getOne(id);
        String sedeCentralString = sc.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Se le da valores a los (?) de la QUERY
        stat.setLong(1, id);

        //Se le da valores a los (?) de la QUERY
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(sedeCentralString);
        SQLManager.statClose(stat);
    }

    /**
     * Este método devuelve un OBJETO JAVA solicitado por la QUERY
     * @param rs ResultSet
     * @return Java Object
     * @throws SQLException
     */
    private SedeCentral_Transformer convertirREGISTROaOBJETO(ResultSet rs) throws SQLException {
        String nom  = rs.getString("nombreSede");
        String ubi  = rs.getString("ubicacion");
        SedeCentral_Transformer sedeCentralTransformer = new SedeCentral_Transformer(nom, ubi);
        sedeCentralTransformer.setIdONG(rs.getLong("idONG"));
        return sedeCentralTransformer;
    }

    @Override
    public SedeCentral_Transformer getOne(Long id) throws SQLException {
        //Se crea el nuevo OBJETO.
        SedeCentral_Transformer sedeCentralTransformer = null;

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETONE);
        stat.setLong(1, id);
        rs = stat.executeQuery();

        //Se agrega el REGISTRO extraido desde la BBDD al nuevo OBJETO
        if (rs.next()) sedeCentralTransformer = convertirREGISTROaOBJETO(rs);
        else throw new SQLException("No se ha encontrado ese registro");

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Devolución de los resultados:
        //System.out.println(SQLManager.resultadoGETONE(sedeCentral));
        return sedeCentralTransformer;
    }

    @Override
    public List<SedeCentral_Transformer> getAll() throws SQLException {
        //Se crea el nuevo OBJETO.
        List<SedeCentral_Transformer> sedesCentrales = new ArrayList<>();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETALL);
        rs = stat.executeQuery();

        //Se agregan todos los REGISTROS extraidos desde la BBDD a la nueva LISTA
        while (rs.next()){
            sedesCentrales.add(convertirREGISTROaOBJETO(rs));
        }

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Devolución de los resultados:
        SQLManager.imprimirResultadoGETALL(sedesCentrales);
        return sedesCentrales;
    }
}