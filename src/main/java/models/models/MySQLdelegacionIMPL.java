package models.models;

import models.transformers.Delegacion_Transformer;
import dao.DelegacionDAO;
import jakarta.xml.bind.JAXBException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLdelegacionIMPL implements DelegacionDAO {
    // ---------------------------------- ENTORNO ---------------------------------- //
    // QUERYES
    final String INSERT = "INSERT INTO bbdd_ong (nombreSede, ubicacion) VALUES (?, ?);";
    final String INHIJO = "INSERT INTO bbdd_delegacion(idOng) VALUES (?);";
    final String UPDATE = "UPDATE bbdd_ong SET nombreSede = ?, ubicacion = ? WHERE idOng = ?;";
    final String DELETE = "DELETE FROM bbdd_ong WHERE idOng = ?";
    final String GETONE = "CALL GETONE_BBDD_DELEGACION(?)";
    final String GETALL = "CALL GETALL_BBDD_DELEGACION()";

    // DECLARACIÓN DE ATRIBUTOS GLOBALES
    private Connection conn;
    private PreparedStatement stat;
    private ResultSet rs;

    // MÉTODO CONSTRUCTOR
    public MySQLdelegacionIMPL (Connection conn){
        this.conn = conn;
    }

    // -------------------------------- MÉTODOS CRUD -------------------------------- //
    @Override
    public void insert(Delegacion_Transformer delegacionTransformer) throws JAXBException, SQLException {
        //Se guarda la QUERY en el método prepareStatement.
        stat = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

        //Se ejecuta la QUERY para la entidad PADRE dando valores a sus campos.
        stat.setString(1, delegacionTransformer.getNombreSede());
        stat.setString(2, delegacionTransformer.getUbicacion());
        SQLManager.executeUpdate(stat);
        //Se asigna la Key al objeto autoincrementandola desde la BBDD.
        rs = SQLManager.generateKey(stat);
        delegacionTransformer.setIdONG(rs.getLong(1));
        //Se cierra el Statement y el ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Se ejecuta la QUERY para la entidad HIJO dando valores a sus campos.
        stat = conn.prepareStatement(INHIJO);
        stat.setLong(1, delegacionTransformer.getIdONG());
        SQLManager.executeUpdate(stat);
        //Se cierra el Statement.
        SQLManager.imprimirResultadoINSERT(delegacionTransformer);
        SQLManager.statClose(stat);
    }

    @Override
    public void update(Delegacion_Transformer delegacionTransformer) throws JAXBException, SQLException {
        //Se guarda la QUERY en el método prepareStatement.
        stat = conn.prepareStatement(UPDATE);

        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setString(1, delegacionTransformer.getNombreSede());
        stat.setString(2, delegacionTransformer.getUbicacion());
        stat.setLong(3, delegacionTransformer.getIdONG());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se cierra el Statement.
        SQLManager.imprimirResultadoUPDATE(delegacionTransformer);
        SQLManager.statClose(stat);
    }

    @Override
    public void delete(Delegacion_Transformer delegacionTransformer) throws JAXBException, SQLException {
        String delegacionString = delegacionTransformer.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setLong(1, delegacionTransformer.getIdONG());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(delegacionString);
        SQLManager.statClose(stat);
    }

    @Override
    public void deleID(Long id) throws SQLException {
        //Almacenamos el Objeto que se va a eliminar en un STRING para imprimir luego su resultado por consola.
        Delegacion_Transformer del = this.getOne(id);
        String delegacionString = del.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setLong(1, id);

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(delegacionString);
        SQLManager.statClose(stat);
    }

    private Delegacion_Transformer convertirREGISTROaOBJETO(ResultSet rs) throws SQLException {
        String nom  = rs.getString("nombreSede");
        String ubi  = rs.getString("ubicacion");
        Delegacion_Transformer delegacionTransformer = new Delegacion_Transformer(nom, ubi);
        delegacionTransformer.setIdONG(rs.getLong("idONG"));
        return delegacionTransformer;
    }

    @Override
    public Delegacion_Transformer getOne(Long id) throws SQLException {
        //Se crea el nuevo OBJETO.
        Delegacion_Transformer delegacionTransformer = null;

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETONE);
        stat.setLong(1, id);
        rs = stat.executeQuery();

        //Se agrega el REGISTRO extraido desde la BBDD al nuevo OBJETO
        if (rs.next()) delegacionTransformer = convertirREGISTROaOBJETO(rs);
        else throw new SQLException("No se ha encontrado ese registro");

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Se devuelve el resultado y se imprimen esté por consola.
        //SQLManager.imprimirResultadoGETONE(delegacion);
        return delegacionTransformer;
    }

    @Override
    public List<Delegacion_Transformer> getAll() throws SQLException {
        //Se crea el nuevo OBJETO que contentrá el ARRAY con los REGISTROS.
        List<Delegacion_Transformer> delegaciones = new ArrayList<>();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETALL);

        //Se ejecuta la QUERY (que contiene nuestro PROCEDIMIENTO ALMACENADO)
        //y se almacena el resultado devuelto en un objeto de tipo ResultSet
        rs = stat.executeQuery();

        //Se pasan uno por uno todos los REGISTROS extraidos desde la BBDD
        //a la LISTA instanciada para esté fin, haciendo para ello, uso de
        //nuestro método privado CONVERTIR_REGISTRO_A_OBJETO.
        while (rs.next()){
            delegaciones.add(convertirREGISTROaOBJETO(rs));
        }

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Se devuelven los resultados y se imprimen estos por consola.
        SQLManager.imprimirResultadoGETALL(delegaciones);
        return delegaciones;
    }
}

