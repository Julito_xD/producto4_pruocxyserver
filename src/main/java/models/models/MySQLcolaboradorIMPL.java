package models.models;

import models.transformers.Colaborador_Transformer;
import dao.ColaboradorDAO;
import jakarta.xml.bind.JAXBException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static models.models.SQLManager.*;
import static models.transformers.Equipo_Transformer.*;

public class MySQLcolaboradorIMPL implements ColaboradorDAO {

    //ENUM
    //public enum Rol { USUARIO, ADMINISTRADOR}

    // QUERIES
    final String CREATEEQUIPO=      "INSERT INTO db_spm.equipo (nombre, apellido1, apellido2, telefono, usuario, password, altaMiembroEquipo, " +
                                    "bajaMiembroEquipo, rol, idOng) VALUES\n" +
                                    "(?, ?, ?, ?, ?, ?, ?, ?, ?,?)";

    final String CREATECOLABORADOR = "INSERT INTO colaborador (tipoColaboracion, idEquipo) VALUES (?,?)";

    final String UPDATEEQUIPO =     "UPDATE db_spm.equipo SET nombre = ?, " +
                                    "apellido1 = ?, apellido2 = ?, telefono = ?, usuario = ?, password = ?, altaMiembroEquipo = ?, " +
                                    "bajaMiembroEquipo = ?, rol = ?, idONg = ? " +
                                    "WHERE idEquipo = ?;";

    final String UPDATECOLABORADOR = "UPDATE db_spm.colaborador SET tipoColaboracion = ? WHERE idEquipo = ?;";

    final String DELETE =           "DELETE FROM equipo WHERE idEquipo = ?";

    final String GETALL =           "SELECT * FROM db_spm.colaborador INNER JOIN db_spm.equipo ON colaborador.idEquipo = equipo.idEquipo";

    final String GETONE = GETALL +  " WHERE colaborador.idEquipo = ?";

    // GLOBAL ATTRIBUTES
    private final Connection connection;
    private PreparedStatement stat;
    private ResultSet rs;

    // CONSTRUCTOR METHOD
    public MySQLcolaboradorIMPL(Connection connection){
        this.connection = connection;
    }

    //CRUD METHODS
    @Override
    public void insert(Colaborador_Transformer a) throws JAXBException, SQLException {

        //Query in prepareStatement method
        stat = connection.prepareStatement(CREATEEQUIPO, Statement.RETURN_GENERATED_KEYS);

        //Executing the CREATEEQUIPO query [father]. We give it values
        createQueryFather(a);

        //Executing
        executeUpdate(stat);

        //Assigning Key to the object incrementing it from de DB
        rs = generateKey(stat);
        a.setId(rs.getLong(1));

        //Closing rs and ResultSet
        rsClose(rs);
        statClose(stat);

        //Executing query [son]. We give it values
        stat = connection.prepareStatement(CREATECOLABORADOR);
        stat.setString(1, a.getTipoColaboracion());
        stat.setLong(2, a.getId());

        //Executing
        executeUpdate(stat);

        //Closing rs and ResultSet
        imprimirResultadoINSERT(a);
        rsClose(rs);
        statClose(stat);
    }

    @Override
    public void update(Colaborador_Transformer a) throws JAXBException, SQLException {

        //Query in prepared statement method
        stat = connection.prepareStatement(UPDATEEQUIPO);

        //Executing the UPDATE query. We give it values
        createQueryFather(a);

        //Adding the query from WHERE clause
        stat.setInt(11, Math.toIntExact(a.getId()));

        //Test
        //System.out.printf("Query to execute:" + stat);

        //Executing
        executeUpdate(stat);

        //Closing rs and ResultSet
        rsClose(rs);
        statClose(stat);

        //Executing query [son]. We give it values
        stat = connection.prepareStatement(UPDATECOLABORADOR);
        stat.setString(1, a.getTipoColaboracion());
        //Adding the query from WHERE clause
        stat.setInt(2, Math.toIntExact(a.getId()));

        //Executing to apply changes
        executeUpdate(stat);

        //Printing results and closing statement
        imprimirResultadoUPDATE(a);
        statClose(stat);
    }

    @Override
    public void delete(Colaborador_Transformer a) throws JAXBException, SQLException {
        //Saving toString before deleting the object
        String colaboradorString = a.toString();

        //Query in prepared statement method
        stat = connection.prepareStatement(DELETE);

        //Executing query. We give it values
        stat.setLong(1, a.getId());

        //Executing
        executeUpdate(stat);

        //Printing result and closing statement
        imprimirResultadoDELETE(colaboradorString);
        statClose(stat);
        rsClose(rs);
    }

    @Override
    public void deleID(Long id) throws SQLException {
        /* NOT IMPLEMENTED */
    }

    private Colaborador_Transformer registerToObject(ResultSet rs) throws SQLException{

        //Setting values to atributes
        String nombre = rs.getString("nombre");
        String apellido1 = rs.getString("apellido1");
        String apellido2 = rs.getString("apellido2");
        //Converting Date to LocalDate
        LocalDate altaMiembroEquipo = rs.getDate("altaMiembroEquipo").toLocalDate();
        LocalDate bajaMiembroEquipo = rs.getDate("bajaMiembroEquipo").toLocalDate();
        //Keep setting values to atributes
        String telefono = rs.getString("telefono");
        String usuario = rs.getString("usuario");
        String password = rs.getString("password");
        //We are recieving a String
        String rolString = rs.getString("rol");
        //We convert the String into an enum type data
        Rol rol = Rol.valueOf(rolString);
        String tipoColaboracion = rs.getString("tipoColaboracion");
        Long idONG = rs.getLong("idONG");
        Long id = rs.getLong("idEquipo");

        Colaborador_Transformer colaboradorTransformer = new Colaborador_Transformer(nombre, apellido1, apellido2, id, telefono, usuario, password, altaMiembroEquipo,
                bajaMiembroEquipo, rol, idONG, tipoColaboracion);

        System.out.println(colaboradorTransformer);

        return colaboradorTransformer;
    }

    @Override
    public Colaborador_Transformer getOne(Long id) throws SQLException {
        //Creating new object
        Colaborador_Transformer colaboradorTransformer;

        //Query in prepared statement method
        stat = connection.prepareStatement(GETONE);

        //Executing query. We give it values
        stat.setLong(1, id);
        rs = stat.executeQuery();

        //Adding the DB data to the new object created [colaborador]
        if (rs.next()) colaboradorTransformer = registerToObject(rs);
        else throw new SQLException("No se ha encontrado ese registro.\n");

        //Closing statement and ResultSet
        rsClose(rs);
        statClose(stat);

        //Returning output from Query
        return colaboradorTransformer;
    }

    @Override
    public List<Colaborador_Transformer> getAll() throws SQLException {

        //Creating new object
        List<Colaborador_Transformer> listaColaboradores = new ArrayList<>();

        //Saving Query to prepared statement method. Executing it
        stat = connection.prepareStatement(GETALL);
        rs = stat.executeQuery();

        //Testing
        //System.out.println("He llegado aqui 1\n");

        //Saving extracted data to the ArrayList already created
        while(rs.next()) listaColaboradores.add(registerToObject(rs));

        //Testing
        //System.out.println("He llegado aqui 2\n");

        //Closing connections
        rsClose(rs);
        statClose(stat);

        //Returning output values
        return listaColaboradores;
    }

    public void createQueryFather(Colaborador_Transformer a) throws SQLException {
        stat.setString(1, a.getNombre());
        stat.setString(2, a.getApellido1());
        stat.setString(3, a.getApellido2());
        stat.setString(4, a.getTelefono());
        stat.setString(5, a.getUser());
        stat.setString(6, a.getPassword());
        stat.setDate(7, new Date(LocalDateToLong.getLong(a.getFechaAltaMiembroEquipo())));
        stat.setDate(8, new Date(LocalDateToLong.getLong(a.getFechaBajaMiembroEquipo())));
        stat.setString(9, String.valueOf(a.getRol()));
        stat.setInt(10, Math.toIntExact(a.getIdOng()));
    }
}
