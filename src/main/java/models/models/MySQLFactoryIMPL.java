package models.models;

import dao.DAO_Factory;

import java.sql.Connection;
import java.sql.SQLException;

public class MySQLFactoryIMPL extends DAO_Factory {
    //DECLARACIÓN DE ATRIBUTOS GLOBALES
    Connection conn;

    //SETTER
    public void setConnection(Connection conn) {
        this.conn = conn;
    }

    //FACTORIA
    public MySQLcolaboradorIMPL getColaboradorIMPL() throws SQLException {
        return new MySQLcolaboradorIMPL(conn);
    }

    public MySQLdelegacionIMPL getDelegacionIMPL() throws SQLException {
        return new MySQLdelegacionIMPL(conn);
    }

    public MySQLingresoPrivadoIMPL getIngresoPrivadoIMPL() throws SQLException {
        return new MySQLingresoPrivadoIMPL(conn);
    }

    public MySQLingresoPublicoIMPL getIngresoPublicoIMPL() throws SQLException {
        return new MySQLingresoPublicoIMPL(conn);
    }

    public MySQLlineaAccionIMPL getLineaAccionIMPL() throws SQLException {
        return new MySQLlineaAccionIMPL(conn);
    }

    public MySQLPersonaContratadaIMPL getPersonaContratadaIMPL() throws SQLException {
        return new MySQLPersonaContratadaIMPL();
    }

    public MySQLProyectoIMPL getProyectoIMPL() throws SQLException {
        return new MySQLProyectoIMPL();
    }

    public MySQLsedeCentralIMPL getSedeCentralIMPL() throws SQLException {
        return new MySQLsedeCentralIMPL(conn);
    }

    public MySQLsocioIMPL getSocioIMPL() throws SQLException {
        return new MySQLsocioIMPL(conn);
    }

    public MySQLsublineaAccionIMPL getSublineaAccionIMPL() throws SQLException {
        return new MySQLsublineaAccionIMPL(conn);
    }

    public MySQLVoluntarioIMPL getVoluntarioIMPL() throws SQLException {
        return new MySQLVoluntarioIMPL();
    }

    public MySQLVoluntarioInternacionalIMPL getVoluntarioInternacionalIMPL() throws SQLException {
        return new MySQLVoluntarioInternacionalIMPL();
    }
}
