package models.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class SQLManager {
    /* ============================================================================= //
    // -------------------------------- EXCEPCIONES -------------------------------- //
    // ============================================================================= */

    /**
     * Manejo de execepciones del método executeUpdate()
     * @param statement
     * @throws SQLException
     */
    public static void executeUpdate(PreparedStatement statement) throws SQLException {
        if (statement.executeUpdate() == 0 ){
            System.out.println("Puede que el objeto no se halla registrado en la BBDD");
        }
    }

    /**
     * Devuelve una PrimaryKey generada por el SGBD almacenandola en un objeto
     * de tipo ResultSet, y controla el manejo de excepciones de este método.
     * @param statement
     * @return
     * @throws SQLException
     */
    public static ResultSet generateKey(PreparedStatement statement) throws SQLException {
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()){
            return rs;
        } else {
            System.out.println("No se puede asignar ID a este objeto");
        }
        return null;
    }

    /**
     * Cierra la conexión otorgada al objeto de tipo STATEMENT y
     * controla el manejo de excepciones de dicho cierre.
     * @param statement
     * @throws SQLException
     */
    public static void statClose(PreparedStatement statement) throws SQLException {
        if (statement != null){
            try{
                statement.close();
            }catch (SQLException ex){
                throw new SQLException("Error al tratar de cerrar el Statement", ex);
            }
        }
    }

    /**
     * Cierra la conexión otorgada al objeto de tipo RESULTSET y
     * controla el manejo de excepciones de dicho cierre.
     * @param rs
     * @throws SQLException
     */
    public static void rsClose(ResultSet rs) throws SQLException {
        if (rs != null){
            try{
                rs.close();
            }catch (SQLException ex){
                throw new SQLException("Error al tratar de cerrar el ResultSet", ex);
            }
        }
    }

    /* ============================================================================ //
    // -------------------------------- RESULTADOS -------------------------------- //
    // ============================================================================ */

    public static void imprimirResultadoINSERT(Object object){
        System.out.println("---\nEl objeto " +object+ "\nse ha añadido a la BBDD, exitosamente.");
    }

    public static void imprimirResultadoUPDATE(Object object){
        System.out.println("---\nEl objeto " +object+ "\nse ha actualizado exitosamente.");
    }

    public static void imprimirResultadoDELETE(String objectString){
        System.out.println("---\nEl objeto " +objectString+ "\nse ha eliminado exitosamente de la BBDD.");
    }

    public static void imprimirResultadoGETONE(Object object){
        System.out.println("---\nSe ha obtenido correctamente el registro:\n"+ object);
    }

    public static void imprimirResultadoGETALL(List objectList){
        System.out.println("---\nLa BBDD contiene, los siguientes registros:");
        int i = 0;
        while (i < objectList.size()){
            System.out.println(objectList.get(i));
            i++;
        }
    }
}
