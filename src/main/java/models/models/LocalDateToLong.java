package models.models;


import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class LocalDateToLong {
    public static long getLong(LocalDate localdate) {
        ZoneId zoneId = ZoneId.systemDefault();
        return localdate.atStartOfDay(zoneId).toEpochSecond();
    }

    public static LocalDate getLocalDate(Date date){
        ZoneId zoneId = ZoneId.systemDefault();
        return date.toInstant().atZone(zoneId).toLocalDate();
    }
}
/* ----------------------- CÓDIGOS DESCARTADOS ----------------------- //
    public long createTimestamp() {
        ZoneId zoneId = ZoneId.systemDefault();
        return LocalDateTime.now().atZone(zoneId).toEpochSecond();
    }
---------------------------------------------------------------------- */