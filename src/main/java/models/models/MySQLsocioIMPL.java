package models.models;

import models.transformers.Socio_Transformer;
import dao.SocioDAO;
import jakarta.xml.bind.JAXBException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MySQLsocioIMPL implements SocioDAO {
    // ---------------------------------- ENTORNO ---------------------------------- //
    // QUERYES
    /*final String INSERT = "INSERT INTO socio (IdSocio,Nombre,Apellido1,Apellido2,NIF,CuentaBancaria,FechaAlta,FechaBaja,tipoCuota,idONG VALUES (?,?,?,?,?,?,?,?,?,?)";
    final String UPDATE = "";
    final String DELETE = "";
    final String GETALL = "";
    final String GETONE = "";*/

    final String INSERT = "INSERT INTO socio (idSocio,nombre,apellido1,apellido2,nIF,cuentaBancaria,fechaAlta,fechaBaja,tipoCuota, idONG) VALUES (?,?,?,?,?,?,?,?,?,?);";
    final String UPDATE = "UPDATE socio SET nombre = ?, apellido1 = ?, apellido2 = ?, nIF = ?, cuentaBancaria = ?, fechaAlta = ?, fechaBaja = ?, tipoCuota = ?, idONG = ? WHERE idSocio = ?;";
    final String DELETE = "DELETE FROM socio WHERE IdSocio = ?";
    final String GETALL = "SELECT * FROM db_spm.socio";
    final String GETONE =  GETALL + " WHERE socio.idSocio = ?";

    // DECLARACIÓN DE ATRIBUTOS GLOBALES
    private Connection conn;
    private PreparedStatement stat;
    private ResultSet rs;

    // CONSTRUCTOR
    public MySQLsocioIMPL(Connection conn){
        this.conn = conn;
    }

    //-------- ESTABLECIENDO LA  CONEXIÓN CON LA BD ---------


    //Connection conn = JDBC.claseUtilidad.getConexion();




   // -------------------------------- MÉTODOS CRUD -------------------------------- //
    @Override
    public void insert(Socio_Transformer socioTransformer) throws JAXBException, SQLException {
        //Guardamos la QUERY en el método prepareStatement.
        stat = conn.prepareStatement(INSERT,Statement.RETURN_GENERATED_KEYS);

        //Añadimos los campos de la entidad.
        stat.setLong(1, socioTransformer.getId());
        stat.setString(2, socioTransformer.getNombre());
        stat.setString(3, socioTransformer.getApellido1());
        stat.setString(4, socioTransformer.getApellido2());
        stat.setString(5, socioTransformer.getDni());
        stat.setString(6, socioTransformer.getCuentaBancaria());
        stat.setDate(7,new Date(LocalDateToLong.getLong(socioTransformer.getFechaAltaSocio())));
        stat.setDate(8,new Date(LocalDateToLong.getLong(socioTransformer.getFechaBajaSocio())));
        stat.setString(9, String.valueOf(socioTransformer.getTipoCuota()));
        stat.setLong(10, socioTransformer.getOng());

        //Ejecutamos el código.

        SQLManager.executeUpdate(stat);
        //Se asigna la Key al objeto autoincrementandola desde la BBDD.
        rs = SQLManager.generateKey(stat);
        socioTransformer.setId(rs.getLong(1));
        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);
        SQLManager.imprimirResultadoINSERT(socioTransformer);

    }

    @Override
    public void update(Socio_Transformer socioTransformer) throws JAXBException, SQLException {

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(UPDATE);

        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setLong(1, socioTransformer.getId());
        stat.setString(2, socioTransformer.getNombre());
        stat.setString(3, socioTransformer.getApellido1());
        stat.setString(4, socioTransformer.getApellido2());
        stat.setString(5, socioTransformer.getDni());
        stat.setString(6, socioTransformer.getCuentaBancaria());
        stat.setDate(7,new Date(LocalDateToLong.getLong(socioTransformer.getFechaAltaSocio())));
        stat.setDate(8,new Date(LocalDateToLong.getLong(socioTransformer.getFechaBajaSocio())));
        stat.setString(9, String.valueOf(socioTransformer.getTipoCuota()));
        stat.setLong(10, socioTransformer.getOng());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoUPDATE(socioTransformer);
        SQLManager.statClose(stat);



    }

    @Override
    public void deleID(Long id) throws SQLException, JAXBException {
        //Almacenamos el Objeto que se va a eliminar en un STRING para imprimir luego su resultado por consola.
        Socio_Transformer socioTransformer = this.getOne(id);
        String socioString = socioTransformer.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos los nuevos valores a los campos de la Entidad. -----------  POR QUÉ AÑADIMOS VALORES SI SE ELIMINARÁ??? -----
        stat.setLong(1, id);

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(socioString);
        SQLManager.statClose(stat);
    }

    @Override
    public void delete(Socio_Transformer socioTransformer) throws JAXBException, SQLException {
        String socioString = socioTransformer.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos los nuevos valores a los campos de la Entidad. -----------  POR QUÉ AÑADIMOS VALORES SI SE ELIMINARÁ??? -----
        stat.setLong(1, socioTransformer.getId());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(socioString);
        SQLManager.statClose(stat);
    }

    private Socio_Transformer convertirREGISTROaOBJETO(ResultSet rs) throws SQLException {

        String nombre = rs.getString("nombre");
        String apellido1 = rs.getString("apellido1");
        String apellido2 = rs.getString("apellido2");
        String dni = rs.getString("NIF");
        String cuentaBancaria = rs.getString("cuentaBancaria");
        LocalDate fechaAlta = LocalDate.parse(rs.getString("fechaAlta"));
        LocalDate fechaBaja = LocalDate.parse(rs.getString("fechaBaja"));
        String tipoCuota = rs.getString("tipoCuota");
        Long idOng = rs.getLong("idOng");
        Socio_Transformer socioTransformer = new Socio_Transformer(nombre, apellido1, apellido2, dni, cuentaBancaria, fechaAlta, fechaBaja, tipoCuota, idOng);
        socioTransformer.setId(rs.getLong("idSocio"));
        return socioTransformer;
    }

    @Override
    public Socio_Transformer getOne(Long id) throws SQLException {

        //Se crea el nuevo OBJETO.
        Socio_Transformer socioTransformer = null;

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETONE);
        stat.setLong(1, id);
        rs = stat.executeQuery();

        //Se agrega el REGISTRO extraido desde la BBDD al nuevo OBJETO
        if (rs.next()) socioTransformer = convertirREGISTROaOBJETO(rs);
        else throw new SQLException("No se ha encontrado ese registro");

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);
        SQLManager.imprimirResultadoGETONE(socioTransformer);

        //Devolución de los resultados:
        //System.out.println(SQLManager.resultadoGETONE(sublineaAccion)
        return socioTransformer;

    }

    @Override
    public List<Socio_Transformer> getAll() throws SQLException {
        //Se crea el nuevo OBJETO.
        List<Socio_Transformer> socioTransformers = new ArrayList<>();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETALL);
        rs = stat.executeQuery();

        //Se agregan todos los REGISTROS extraidos desde la BBDD a la nueva LISTA
        while (rs.next()){
            socioTransformers.add(convertirREGISTROaOBJETO(rs));
        }

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Devolución de los resultados:
        SQLManager.imprimirResultadoGETALL(socioTransformers);
        return socioTransformers;
    }
}
