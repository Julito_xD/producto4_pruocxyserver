package models.models;

import models.transformers.LineaAccion_Transformer;
import dao.LineaAccionDAO;
import jakarta.xml.bind.JAXBException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 //Implementación del DAO para la clase LineaAccion para MySQL
 */

public class MySQLlineaAccionIMPL implements LineaAccionDAO {
    // ---------------------------------- ENTORNO ---------------------------------- //
    // QUERYES
    final String INSERT = "INSERT INTO lineaaccion (idLinea,tipoLinea) VALUES (?, ?);";
    final String UPDATE = "UPDATE lineaaccion SET tipoLinea = ? WHERE idLinea = ?";
    final String DELETE = "DELETE FROM lineaaccion WHERE idLinea = ?";
    final String GETALL = "SELECT * FROM lineaaccion";
    final String GETONE = "SELECT * FROM lineaaccion WHERE idLinea = ?";



    // DECLARACIÓN DE ATRIBUTOS GLOBALES
    private Connection conn;
    private PreparedStatement stat;
    private ResultSet rs;

    // MÉTODO CONSTRUCTOR
    public MySQLlineaAccionIMPL(Connection conn){
        this.conn = conn;
    }

    // -------------------------------- MÉTODOS CRUD -------------------------------- //
    @Override
    public void insert(LineaAccion_Transformer lineaAccionTransformer) throws JAXBException, SQLException {
        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

        //Se ejecuta la QUERY para la entidad PADRE dando valores a sus campos.
        stat.setLong(1, lineaAccionTransformer.getIdLinea());
        stat.setString(2, String.valueOf(lineaAccionTransformer.getTipoLinea()));

        SQLManager.executeUpdate(stat);
        //Se asigna la Key al objeto autoincrementandola desde la BBDD.
        rs = SQLManager.generateKey(stat);
        lineaAccionTransformer.setIdLinea(rs.getLong(1));
        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);
        SQLManager.imprimirResultadoINSERT(lineaAccionTransformer);
    }

    @Override
    public void update(LineaAccion_Transformer lineaAccionTransformer) throws JAXBException, SQLException {
        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(UPDATE);

        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setString(1, String.valueOf(lineaAccionTransformer.getTipoLinea()));
        stat.setLong(2, lineaAccionTransformer.getIdLinea());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoUPDATE(lineaAccionTransformer);
        SQLManager.statClose(stat);
    }


    @Override
    public void delete(LineaAccion_Transformer lineaAccionTransformer) throws JAXBException, SQLException {
        String LineaAccionString = lineaAccionTransformer.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setLong(1, lineaAccionTransformer.getIdLinea());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(LineaAccionString);
        SQLManager.statClose(stat);
    }

    @Override
    public void deleID(Long id) throws SQLException {
        //Almacenamos el Objeto que se va a eliminar en un STRING para imprimir luego su resultado por consola.
        LineaAccion_Transformer sc = this.getOne(id);
        String lineaAccionString = sc.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setLong(1, id);

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(lineaAccionString);
        SQLManager.statClose(stat);
    }

    private LineaAccion_Transformer convertirREGISTROaOBJETO(ResultSet rs) throws SQLException {
        LineaAccion_Transformer lineaAccionTransformer = new LineaAccion_Transformer();

        lineaAccionTransformer.setIdLinea(rs.getLong("idLinea"));
        lineaAccionTransformer.setTipoLinea(LineaAccion_Transformer.tipoLinea.valueOf(rs.getString("tipoLinea")));

        return lineaAccionTransformer;
    }

    @Override
    public LineaAccion_Transformer getOne(Long id) throws SQLException {
        //Se crea el nuevo OBJETO.
        LineaAccion_Transformer lineaAccionTransformer = null;

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETONE);
        stat.setLong(1, id);
        rs = stat.executeQuery();

        //Se agrega el REGISTRO extraido desde la BBDD al nuevo OBJETO
        if (rs.next()) lineaAccionTransformer = convertirREGISTROaOBJETO(rs);
        else throw new SQLException("No se ha encontrado ese registro");

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Devolución de los resultados:
        //System.out.println(SQLManager.resultadoGETONE(lineaAccion));
        return lineaAccionTransformer;
    }

    @Override
    public List<LineaAccion_Transformer> getAll() throws SQLException {
        //Se crea el nuevo OBJETO.
        List<LineaAccion_Transformer> lineasAcciones = new ArrayList<>();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETALL);
        rs = stat.executeQuery();

        //Se agregan todos los REGISTROS extraidos desde la BBDD a la nueva LISTA
        while (rs.next()){
            lineasAcciones.add(convertirREGISTROaOBJETO(rs));
        }

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Devolución de los resultados:
        SQLManager.imprimirResultadoGETALL(lineasAcciones);
        return lineasAcciones;
    }
}
