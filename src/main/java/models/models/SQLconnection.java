package models.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLconnection {
    //DECLARACIÓN DE CLASE
    private static Connection conexion = null;
    //Atributos de la URL
    private String usuario  = "root";
    private String password = "1234";
    private String puerto   = "3309";
    private String nombreBD = "db_spm";

    private String url      = "jdbc:mysql://localhost:"+puerto+"/"+nombreBD;

    // ---------------------------- CONSTRUCTOR ---------------------------- //
    /**
     * Metodo constructor que llama a un método privado que establece
     * la conexión con la BBDD declarada en los los parámetros de la URL.
     * @throws SQLException
     */
    public SQLconnection() throws SQLException {
        conectarBBDD();
    }

    // ------------------------- GETTERS Y SETTERS ------------------------- //

    /**
     * Devuelve el objeto con la conexión realizada contra la BBDD
     * @return conexión contra la BBDD
     */
    public static Connection getConexion(){
        return conexion;
    }

    // ------------------------- MÉTODOS DE CLASE -------------------------- //

    /**
     * Establece la conexión contra la BBDD establecida en los parametros de la URO
     * y lanza un mensaje de exit si conecta, y una excepción SQL, si la conexión falla.
     * @throws SQLException
     */
    private void conectarBBDD() throws SQLException {
        //Establecemos la conexión con la BBDD.
        conexion = DriverManager.getConnection(url, usuario, password);
        //Entrega el resultado de la conexión.
        System.out.println("Te has conectado exitosamente a la BBDD: 'Sonrisas por el Mundo'");
    }
}

