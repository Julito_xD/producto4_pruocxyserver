package models.models;

import models.transformers.IngresoPublico_Transformer;
import dao.IngresoPublicoDAO;
import jakarta.xml.bind.JAXBException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementación del DAO para la clase IngresoPublico para MySQL
 * @author laurazp
 */

public class MySQLingresoPublicoIMPL implements IngresoPublicoDAO {
    // ---------------------------------- ENTORNO ---------------------------------- //
    // QUERIES
    final String INSERT = "INSERT INTO db_spm.INGRESO (cantidad, tipoAportacion, fechaIngreso, nombreAportador, idAportador, idONG) " +
            "VALUES (?, ?, ?, ?, ?, ?);";
    final String INHIJO = "INSERT INTO db_spm.INGRESO_PUBLICO (idIngreso, tipoAportador) VALUES (?, ?);";
    final String UPDATE = "UPDATE db_spm.INGRESO SET cantidad = ?, tipoAportacion = ?, fechaIngreso = ?, nombreAportador = ?, " +
                          "idAportador = ?, idONG = ?, tipoAportador = ? WHERE idIngreso = ?;";
    final String DELETE = "DELETE FROM db_spm.INGRESO WHERE idIngreso = ?;";
    final String GETONE = "CALL GETONE_INGRESO_PUBLICO(?);";
    final String GETALL = "CALL GETALL_INGRESO_PUBLICO();";
    //-------- Métodos CRUD UPDATE opcionales de la clase IngresoPublico que modifican sólo un parámetro
    final String UPD_CANTIDAD = "UPDATE db_spm.INGRESO SET cantidad = ? WHERE idIngreso = ?;";
    final String UPD_NOMAPORT = "UPDATE db_spm.INGRESO SET nombreAportador = ? WHERE idIngreso = ?;";


    // DECLARACIÓN DE ATRIBUTOS GLOBALES
    private Connection conn;
    private PreparedStatement stat;
    private ResultSet rs;

    // MÉTODO CONSTRUCTOR
    public MySQLingresoPublicoIMPL(Connection conn){
        this.conn = conn;
    }

    // -------------------------------- MÉTODOS CRUD -------------------------------- //
    @Override
    public void insert(IngresoPublico_Transformer ingresoPublicoTransformer) throws JAXBException, SQLException {
        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

        //Se ejecuta la QUERY para la entidad PADRE dando valores a sus campos.
        stat.setFloat(1, ingresoPublicoTransformer.getCantidad ());
        stat.setObject(2, ingresoPublicoTransformer.getTipoAportacion());
        //stat.setObject(2, ingresoPublico.getTipoAportacion().valueOf(rs.getString("tipoAportador")));
        //stat.setObject(2, Ingreso.TipoAportacion.AP_PUNTUAL);
        stat.setDate(3, new Date(LocalDateToLong.getLong(ingresoPublicoTransformer.getFechaIngreso())));
        stat.setString(4, ingresoPublicoTransformer.getNombreAportador());
        stat.setString(5, ingresoPublicoTransformer.getIdAportador());
        stat.setLong(6, ingresoPublicoTransformer.getIdOng());
        SQLManager.executeUpdate(stat);
        //Se asigna la Key al objeto autoincrementandola desde la BBDD.
        rs = SQLManager.generateKey(stat);
        ingresoPublicoTransformer.setIdIngreso(rs.getLong(1));
        //Se cierra la conexion del Statement y la conexión del ResultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Se ejecuta la QUERY para la entidad HIJO dando valores a sus campos.
        stat = conn.prepareStatement(INHIJO);
        stat.setLong(1, ingresoPublicoTransformer.getIdIngreso());
        stat.setObject(2, ingresoPublicoTransformer.getTipoAportador());
        SQLManager.executeUpdate(stat);
        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoINSERT(ingresoPublicoTransformer);
        SQLManager.statClose(stat);
    }

    @Override
    public void update(IngresoPublico_Transformer ingresoPublicoTransformer) throws JAXBException, SQLException {
        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(UPDATE);

        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setFloat(1, ingresoPublicoTransformer.getCantidad ());
        stat.setObject(2, ingresoPublicoTransformer.getTipoAportacion());
        stat.setDate(3, new Date(LocalDateToLong.getLong(ingresoPublicoTransformer.getFechaIngreso())));
        stat.setString(4, ingresoPublicoTransformer.getNombreAportador());
        stat.setString(5, ingresoPublicoTransformer.getIdAportador());
        stat.setLong(6, ingresoPublicoTransformer.getIdOng());
        stat.setObject(7, ingresoPublicoTransformer.getTipoAportador());
        stat.setLong(8, ingresoPublicoTransformer.getIdIngreso());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoUPDATE(ingresoPublicoTransformer);
        SQLManager.statClose(stat);
    }

    @Override
    public void delete(IngresoPublico_Transformer ingresoPublicoTransformer) throws JAXBException, SQLException {
        String ingresoPublicoString = ingresoPublicoTransformer.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos el valor de la ID de la Entidad que queremos eliminar.
        stat.setLong(1, ingresoPublicoTransformer.getIdIngreso());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(ingresoPublicoString);
        SQLManager.statClose(stat);
    }

    @Override
    public void deleID(Long id) throws SQLException {
        //Almacenamos el Objeto que se va a eliminar en un STRING para imprimir luego su resultado por consola.
        IngresoPublico_Transformer ingPub = this.getOne(id); //Usamos el método GETONE para acceder al objeto a partir de su ID
        String ingresoPublicoString = ingPub.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos el valor de la ID de la Entidad que queremos eliminar.
        stat.setLong(1, id);

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(ingresoPublicoString);
        SQLManager.statClose(stat);
    }

    private IngresoPublico_Transformer convertirREGISTROaOBJETO(ResultSet rs) throws SQLException {
        //Se transforman los campos de la tabla en atributos del objeto
        float cantidad  = rs.getFloat("cantidad");
        String tipoAportacion  = rs.getString("tipoAportacion");
        LocalDate fechaIngreso  = LocalDate.parse(rs.getString("fechaIngreso"));
        String nombreAportador  = rs.getString("nombreAportador");
        String idAportador  = rs.getString("idAportador");
        Long idOng = rs.getLong("idOng");
        String tipoAportador  = rs.getString("tipoAportador");
        //Se instancia el objeto
        IngresoPublico_Transformer ingresoPublicoTransformer = new IngresoPublico_Transformer(cantidad, tipoAportacion, fechaIngreso, nombreAportador,
                                idAportador, idOng, tipoAportador);
        ingresoPublicoTransformer.setIdIngreso(rs.getLong("idIngreso"));
        return ingresoPublicoTransformer;
    }

    @Override
    public IngresoPublico_Transformer getOne(Long id) throws SQLException {
        //Se crea el nuevo OBJETO.
        IngresoPublico_Transformer ingresoPublicoTransformer = null;

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETONE);
        stat.setLong(1, id);
        rs = stat.executeQuery();

        //Se agrega el REGISTRO extraido desde la BBDD al nuevo OBJETO
        if (rs.next()) ingresoPublicoTransformer = convertirREGISTROaOBJETO(rs);
        else throw new SQLException("No se ha encontrado ese registro");

        //Se cierra la conexión del Statement y la conexión del ResultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Devolución de los resultados:
        //System.out.println(SQLManager.resultadoGETONE(ingresoPublico));
        return ingresoPublicoTransformer;
    }

    @Override
    public List<IngresoPublico_Transformer> getAll() throws SQLException {
        //Se crea el nuevo OBJETO.
        List<IngresoPublico_Transformer> ingresosPublicos = new ArrayList<>();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETALL);
        rs = stat.executeQuery();

        //Se agregan todos los REGISTROS extraidos desde la BBDD a la nueva LISTA, convirtiéndolos a objetos.
        while (rs.next()){
            ingresosPublicos.add(convertirREGISTROaOBJETO(rs));
        }

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Devolución de los resultados:
        SQLManager.imprimirResultadoGETALL(ingresosPublicos);
        return ingresosPublicos;
    }
}
