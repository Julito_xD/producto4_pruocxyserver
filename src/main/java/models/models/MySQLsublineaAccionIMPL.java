package models.models;

import models.transformers.SublineaAccion_Transformer;
import dao.SublineaAccionDAO;
import jakarta.xml.bind.JAXBException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;



/**
 //Implementación del DAO para la clase SublineaAccion para MySQL
 */

public class MySQLsublineaAccionIMPL implements SublineaAccionDAO {
    // ---------------------------------- ENTORNO ---------------------------------- //
    // QUERYES
    final String INSERT = "INSERT INTO sublineaaccion (idSublinea, concepto) VALUES (?, ?);";
    final String DELETE = "DELETE FROM sublineaaccion WHERE idSublinea = ?";
    final String UPDATE = "UPDATE sublineaaccion SET concepto = ?";
    final String GETONE = "CALL GETONE_BBDD_SUBLINEACCION(?)";
    final String GETALL = "CALL GETALL_BBDD_SUBLINEACCION()";

    /*
    Julito es el mejor y ademas de eso
    es la persona más humilde del mundo.
    */


    // DECLARACIÓN DE ATRIBUTOS GLOBALES
    private Connection conn;
    private PreparedStatement stat;
    private ResultSet rs;

    // MÉTODO CONSTRUCTOR
    public MySQLsublineaAccionIMPL(Connection conn){
        this.conn = conn;
    }

    // -------------------------------- MÉTODOS CRUD -------------------------------- //
    @Override
    public void insert(SublineaAccion_Transformer sublineaAccionTransformer) throws JAXBException, SQLException {
        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

        //Se ejecuta la QUERY para la entidad PADRE dando valores a sus campos.
        stat.setLong(1, sublineaAccionTransformer.getIdSublinea());
        stat.setString(2, sublineaAccionTransformer.getConcepto());

        SQLManager.executeUpdate(stat);
        //Se asigna la Key al objeto autoincrementandola desde la BBDD.
        rs = SQLManager.generateKey(stat);
        sublineaAccionTransformer.setIdSublinea(rs.getLong(1));
        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);


    }

    @Override
    public void update(SublineaAccion_Transformer sublineaAccionTransformer) throws JAXBException, SQLException {
        //Se almacena la QUERY gracias al método prepareStatement.

        //  stat = conn.prepareStatement(UPDATE); ??????????????


        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setLong(1, sublineaAccionTransformer.getIdSublinea());

        stat.setString(2, sublineaAccionTransformer.getConcepto());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoUPDATE(sublineaAccionTransformer);
        SQLManager.statClose(stat);
    }


    @Override
    public void delete(SublineaAccion_Transformer sublineaAccionTransformer) throws JAXBException, SQLException {
        String SublineaAccionString = sublineaAccionTransformer.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setLong(1, sublineaAccionTransformer.getIdSublinea());

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(SublineaAccionString);
        SQLManager.statClose(stat);
    }

    @Override
    public void deleID(Long id) throws SQLException {
        //Almacenamos el Objeto que se va a eliminar en un STRING para imprimir luego su resultado por consola.
        SublineaAccion_Transformer sc = this.getOne(id);
        String sublineaAccionString = sc.toString();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(DELETE);

        //Asignamos los nuevos valores a los campos de la Entidad.
        stat.setLong(1, id);

        //Se ejecuta executeUpdate para que se actualicen los cambios
        SQLManager.executeUpdate(stat);

        //Se imprime el resultado por consola y se cierra la conexión del Statement.
        SQLManager.imprimirResultadoDELETE(sublineaAccionString);
        SQLManager.statClose(stat);
    }

    private SublineaAccion_Transformer convertirREGISTROaOBJETO(ResultSet rs) throws SQLException {

        SublineaAccion_Transformer sublineaAccionTransformer = new SublineaAccion_Transformer();
        sublineaAccionTransformer.setIdSublinea(rs.getLong("idSublinea"));
        return sublineaAccionTransformer;
    }

    @Override
    public SublineaAccion_Transformer getOne(Long id) throws SQLException {
        //Se crea el nuevo OBJETO.
        SublineaAccion_Transformer sublineaAccionTransformer = null;

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETONE);
        stat.setLong(1, id);
        rs = stat.executeQuery();

        //Se agrega el REGISTRO extraido desde la BBDD al nuevo OBJETO
        if (rs.next()) sublineaAccionTransformer = convertirREGISTROaOBJETO(rs);
        else throw new SQLException("No se ha encontrado ese registro");

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Devolución de los resultados:
        //System.out.println(SQLManager.resultadoGETONE(sublineaAccion
        return sublineaAccionTransformer;
    }

    @Override
    public List<SublineaAccion_Transformer> getAll() throws SQLException {
        //Se crea el nuevo OBJETO.
        List<SublineaAccion_Transformer> sublineasAcciones = new ArrayList<>();

        //Se almacena la QUERY gracias al método prepareStatement.
        stat = conn.prepareStatement(GETALL);
        rs = stat.executeQuery();

        //Se agregan todos los REGISTROS extraidos desde la BBDD a la nueva LISTA
        while (rs.next()){
            sublineasAcciones.add(convertirREGISTROaOBJETO(rs));
        }

        //Se cierra la conexion del Statement y la conexión del ResaultSet.
        SQLManager.rsClose(rs);
        SQLManager.statClose(stat);

        //Devolución de los resultados:
        SQLManager.imprimirResultadoGETALL(sublineasAcciones);
        return sublineasAcciones;
    }
}
