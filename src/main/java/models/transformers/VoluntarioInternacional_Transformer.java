package models.transformers;

import java.time.LocalDate;

public class VoluntarioInternacional_Transformer extends Voluntario_Transformer {

    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//
    private String pais;

    //--------------------------------- RELACIONES ENTRE CLASES ---------------------------------//
    //NO HAY

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//

    /**
     * Creamos constructor default
     * @param pais residencia/base guardada como String del voluntario internacional
     * @param nombre nombre del voluntario internacional
     * @param apellido1 primer apellido
     * @param apellido2 segundo apellido
     * (ambos parametros: apellidos del voluntario internacional)
     * @param telefono número de contacto del voluntario internacional
     * @param user usuario de incio de sesión
     * @param fechaAltaMiembroEquipo fecha de inicio en equipo
     * @param password contraseña del usuario del inicio de sesión
     * @param horasPorSemana horas trabajadas por semana
     * @param rol atributo proviniente de la clase Rol. Rol del objeto voluntario internacional,
     * que puede tratarse de USUARIO o ADMINISTRADOR
     */
    public VoluntarioInternacional_Transformer(String nombre, String apellido1, String apellido2, String telefono,
                                               String user, String password, LocalDate fechaAltaMiembroEquipo, LocalDate fechaBajaMiembroEquipo, Rol rol, Long idOng, int horasPorSemana, String pais) {
        super(nombre, apellido1, apellido2, telefono, user, password, fechaAltaMiembroEquipo, fechaBajaMiembroEquipo, rol, idOng, horasPorSemana);
        this.pais = pais;
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//

    public String getPais() {
        return pais;
    }

    public void setPais(String pais){this.pais = pais; }

    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//

    @Override
    public String toString() {
        return "VoluntarioInternacional{" +  "nombre=" + super.getNombre() + ", apellido1=" + super.getApellido1() + ", apellido2=" + super.getApellido2() + ", id=" + super.getId() +
                ", telefono=" + super.getTelefono() + ", user=" + super.getUser() + ", password=" + super.getPassword() + ", fechaAltaMiembroEquipo=" +
                super.getFechaAltaMiembroEquipo() + ", fechaBajaMiembroEquipo=" + super.getFechaBajaMiembroEquipo() + ", rol=" + super.getRol() 
                + " pais=" + pais + '}';
    }
    
}
