package models.transformers;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

public abstract class OrganizacionONG_Transformer {
    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//
    private static final AtomicReference<Long> id = new AtomicReference<>(0L);
    private Long idONG;
    private String nombreSede;
    private String ubicacion;
    //--------------------------------- RELACIONES ENTRE CLASES ---------------------------------//
    private ArrayList<Equipo_Transformer> miembrosEquipoTransformer;
    private ArrayList<Ingreso_Transformer> ingresoTransformers;
    private ArrayList<Socio_Transformer> socioTransformers;
    private ArrayList<Proyecto_Transformer> proyectoTransformers;
    private ArrayList<Delegacion_Transformer> delegacionTransformer;

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//

    /**
     * SOBRECARGA: Creamos un constructor vacío por si en un futuro necesitamos utilizar este recurso
     */
    public OrganizacionONG_Transformer(){
    }

    /**
     * Metodo constructor de la clase OrganizaciónONG para los CRUD XML
     * @param nombreSede
     * @param idONG
     * @param ubicacion
     */
    public OrganizacionONG_Transformer(int idONG, String nombreSede, String ubicacion) {
        this.idONG = Long.valueOf(idONG);
        this.nombreSede = nombreSede;
        this.ubicacion = ubicacion;
    }

    /**
     * Metodo constructor de la clase OrganizaciónONG para los CRUD MySQL
     * @param nombreSede
     * @param ubicacion
     */
    public OrganizacionONG_Transformer(String nombreSede, String ubicacion) {
        this.nombreSede = nombreSede;
        this.ubicacion = ubicacion;
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//

    public Long getIdONG() {
        return idONG;
    }

    public void setIdONG(Long idONG) {
        this.idONG = idONG;
    }

    public String getNombreSede() {
        return nombreSede;
    }

    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public ArrayList<Equipo_Transformer> getMiembrosEquipo() {
        return miembrosEquipoTransformer;
    }

    public ArrayList<Ingreso_Transformer> getIngresos() {
        return ingresoTransformers;
    }

    public ArrayList<Socio_Transformer> getSocios() {
        return socioTransformers;
    }

    public ArrayList<Proyecto_Transformer> getProyectos() {
        return proyectoTransformers;
    }

    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//
    //Añadir nuevo miembrosEquipo
    //Mostrar lista miembrosEquipo

    //Añadir nuevo Ingreso
    //Mostrar totalIngresos

    //Añadir nuevo Socio
    //Mostrar lista socios

    //Añadir nuevo Proyecto
    //Mostrar lista proyectos

    //---------------------------------------- TO STRING ---------------------------------------//

    @Override
    public String toString() {
        return "OrganizacionONG{" +
                "idONG=" + idONG +
                ", nombreSede='" + nombreSede + '\'' +
                ", ubicacion='" + ubicacion + '\'' +
                '}';
    }
}

    //------------------------------------ CÓDIGO DESCARTADO ------------------------------------//

    /*public void crearDelegacion(){

    Scanner sc = new Scanner(System.in);

        System.out.println("Introduce nombre de delegacion");
        String nombreDelegacion = sc.nextLine();

        System.out.println("Introduce numero de codigo");
        int numeroCodigo = sc.nextInt();

        System.out.println("Introduzca la ubicacion");
        String nuevaUbicacion = sc.nextLine();

        Delegacion nuevaDelegacion = new Delegacion(nombreDelegacion, numeroCodigo, nuevaUbicacion);
        delegacion.add(nuevaDelegacion);
    }

    public void modificarDelegacion () {
        for (int i=0;i < delegacion.size();i++ ){
               System.out.println(delegacion.get(i).getNombreSede());
               System.out.println(delegacion.get(i).getCodigo());
               System.out.println(delegacion.get(i).getUbicacion());
            }
        Scanner sc = new Scanner(System.in);
    }    */