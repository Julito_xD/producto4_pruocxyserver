package models.transformers;;

import java.time.LocalDate;
import java.util.ArrayList;

public class Proyecto_Transformer {
    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//
    private Long idProyecto;
    private String pais;
    private String localización;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private String financiador;
    private float financiacionAportada;
    private String accionesRealizar;

    //--------------------------------- RELACIONES ENTRE CLASES ---------------------------------//
    //private OrganizacionONG delegacion;
    private ArrayList<Socio_Transformer> socioTransformers;
    private ArrayList<Equipo_Transformer> miembrosEquipoTransformer;
    //private LineaAccion lineaAccion;

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//

    /**
     * Constructor por defecto, vacio.
     */
    public Proyecto_Transformer() {
    }

    /**
     * Constructor por defecto.
     * @param idProyecto
     * @param pais
     * @param localización
     * @param fechaInicio
     * @param fechaFin
     * @param financiador
     * @param financiacionAportada
     * @param accionesRealizar
     */
    public Proyecto_Transformer(int idProyecto, String pais, String localización, LocalDate fechaInicio, LocalDate fechaFin,
                                String financiador, float financiacionAportada, String accionesRealizar) {
        this.idProyecto = Long.valueOf(idProyecto);
        this.pais = pais;
        this.localización = localización;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.financiador = financiador;
        this.financiacionAportada = financiacionAportada;
        this.accionesRealizar = accionesRealizar;
        //this.delegacion = delegacion;                         //Agregar al constructor más tarde.
        //this.lineaAccion = lineaAccion;                       //Agragar al constructor más tarde.
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//
    public Long getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Long idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getLocalización() {
        return localización;
    }

    public void setLocalización(String localización) {
        this.localización = localización;
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getFinanciador() {
        return financiador;
    }

    public void setFinanciador(String financiador) {
        this.financiador = financiador;
    }

    public float getFinanciacionAportada() {
        return financiacionAportada;
    }

    public void setFinanciacionAportada(float financiacionAportada) {
        this.financiacionAportada = financiacionAportada;
    }

    public String getAccionesRealizar() {
        return accionesRealizar;
    }

    public void setAccionesRealizar(String accionesRealizar) {
        this.accionesRealizar = accionesRealizar;
    }
/*
   public OrganizacionONG getDelegacion() {
        return delegacion;
    }

    public void setDelegacion(OrganizacionONG delegacion) {
        this.delegacion = delegacion;
    }

    public LineaAccion getLineaAccion() {
        return lineaAccion;
    }

    public void setLineaAccion(LineaAccion lineaAccion) {
        this.lineaAccion = lineaAccion;
    }
*/
    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//
    //setAsignarSocio
    //getMostrarListaSocios

    //setAgregarMiembroEquipo
    //getMortarListaEquipo

    //--------------------------------------- TO STRING ---------------------------------------//

    @Override
    public String toString() {
        return "Proyecto{" +
                "idProyecto=" + idProyecto +
                ", pais='" + pais + '\'' +
                ", localización='" + localización + '\'' +
                ", fechaInicio='" + fechaInicio + '\'' +
                ", fechaFin='" + fechaFin + '\'' +
                ", financiador='" + financiador + '\'' +
                ", financiacionAportada=" + financiacionAportada +
                ", accionesRealizar='" + accionesRealizar + '\'' +
                '}';
    }
}
