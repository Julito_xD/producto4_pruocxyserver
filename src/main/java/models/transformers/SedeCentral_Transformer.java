package models.transformers;

public class SedeCentral_Transformer extends OrganizacionONG_Transformer {

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//

    /**
     * Constructor vacio de la clase SedeCentral
     */
    public SedeCentral_Transformer() {
    }

    /**
     * Constructor de la clase SedeCentral para los CRUD XML
     * @param nombreSede
     * @param idONG
     * @param ubicacion
     */
    public SedeCentral_Transformer(int idONG, String nombreSede, String ubicacion) {
        super(idONG, nombreSede, ubicacion);
    }

    /**
     * Constructor de la clase SedeCentral para los CRUD MySQL
     * @param nombreSede
     * @param ubicacion
     */
    public SedeCentral_Transformer(String nombreSede, String ubicacion) {
        super(nombreSede, ubicacion);
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//
    //De momento no es necesario
    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//
    //De momento no es necesario
    //---------------------------------------- TO STRING ---------------------------------------//
    //De momento no es necesario
}
