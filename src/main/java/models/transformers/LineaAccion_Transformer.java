package models.transformers;

import java.util.ArrayList;

public class LineaAccion_Transformer {

    public enum tipoLinea {
        Cooperación_Desarrollo,Acción_Humanitaria, Fortalecimiento_Institucional, Educación_Para_Desarrollo}

    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//
    private Long idLinea;
    private tipoLinea tipoLinea;

    //--------------------------------- RELACIONES ENTRE CLASES ---------------------------------//
    private ArrayList<SublineaAccion_Transformer> sublineasAccion;

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//
    public LineaAccion_Transformer() {
    }

    /**
     * Constructor LineaAccion
     * @param idLinea
     * @param tipoLinea
     */
    public LineaAccion_Transformer(Long idLinea, tipoLinea tipoLinea) {
        this.idLinea = idLinea;
        this.tipoLinea = tipoLinea;
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//

    public Long getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(Long idLinea) {
        this.idLinea = idLinea;
    }

    public LineaAccion_Transformer.tipoLinea getTipoLinea() {
        return tipoLinea;
    }

    public void setTipoLinea(LineaAccion_Transformer.tipoLinea tipoLinea) {
        this.tipoLinea = tipoLinea;
    }

    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//
    //setAgregarSubLineaAccion();
    //getMostrarListaSubLineasAccion();

    //---------------------------------------- TO STRING ---------------------------------------//

    @Override
    public String toString() {
        return "LineaAccion{" +
                "idLinea=" + idLinea +
                ", tipoLinea=" + tipoLinea +
                '}';
    }
}
