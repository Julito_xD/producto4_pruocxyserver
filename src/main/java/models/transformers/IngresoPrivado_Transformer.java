package models.transformers;

import java.time.LocalDate;

public class IngresoPrivado_Transformer extends Ingreso_Transformer {
     /**
     * Definición del tipo enumerado OrigenAportacion que definirá el origen del ingreso privado
     * Tipos de origen para los Ingresos Privados
     */
    public enum OrigenAportacion {INGRESOEXTRA, PARTICULAR, EMPRESA, HERENCIASLEGADOS, INSTITUCIONES, CUOTASOCIO};

    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//
    
    private String origenAportacion;
    private int idSocio;

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//
    
    /**
    * Sobrecarga de métodos constructores - Método constructor por defecto
    */
    public IngresoPrivado_Transformer() {
        
    }
    
    /**
     * Método constructor para los Ingresos Privados para trabajar con XML
     * @param idIngreso Este parámetro es de tipo int y define el identificador del ingreso privado
     * @param cantidad Este parámetro es de tipo float y define la cantidad del ingreso privado en forma decimal
     * @param tipoAportacion Este parámetro es del tipo enumerado TipoAportacion y define el tipo de aportación del ingreso privado
     * @param fechaIngreso Este parámetro es de tipo LocalDate y define la fecha en que se realizó el ingreso privado
     * @param nombreAportador Este parámetro es de tipo String y define el nombre del aportador
     * @param idAportador Este parámetro es de tipo String y define el ID del aportador
     * @param origenAportacion Este parámetro es del tipo enumerado OrigenAportacion y define el origen del ingreso privado
     */
    public IngresoPrivado_Transformer(int idIngreso, float cantidad, String tipoAportacion, LocalDate fechaIngreso,
                                      String nombreAportador, String idAportador, Long idOng, String origenAportacion) {
        super(idIngreso, cantidad, tipoAportacion, fechaIngreso, nombreAportador, idAportador, idOng);
        this.origenAportacion = origenAportacion;
    }

    /**
     * Método constructor para los Ingresos Privados para trabajar con MySQL
     * @param cantidad Este parámetro es de tipo float y define la cantidad del ingreso privado en forma decimal
     * @param tipoAportacion Este parámetro es del tipo enumerado TipoAportacion y define el tipo de aportación del ingreso privado
     * @param fechaIngreso Este parámetro es de tipo LocalDate y define la fecha en que se realizó el ingreso privado
     * @param nombreAportador Este parámetro es de tipo String y define el nombre del aportador
     * @param idAportador Este parámetro es de tipo String y define el ID del aportador
     * @param idOng Este parámetro es de tipo Long y define el ID de la ONG (sede o delegación)
     * @param origenAportacion Este parámetro es del tipo enumerado OrigenAportacion y define el origen del ingreso privado
     */
    public IngresoPrivado_Transformer(float cantidad, String tipoAportacion, LocalDate fechaIngreso,
                                      String nombreAportador, String idAportador, Long idOng, String origenAportacion, int idSocio) {
        super(cantidad, tipoAportacion, fechaIngreso, nombreAportador, idAportador, idOng);
        this.origenAportacion = origenAportacion;
        this.idSocio = idSocio;
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//

    public String getOrigenAportacion() {
        return origenAportacion;
    }

    public void setOrigenAportacion(String origenAportacion) {
        this.origenAportacion = origenAportacion;
    }

    public int getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(int idSocio) {
        this.idSocio = idSocio;
    }

    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//



    //---------------------------------------- TO STRING ---------------------------------------//
    /**
     * Método que permite mostrar la información completa de un objeto de la clase IngresoPrivado
     * @return Devuelve un String con todos los datos del ingreso privado
     */
    @Override
    public String toString() {
        return "Ingreso Privado{" + "idIngreso=" + super.getIdIngreso() + ", cantidad=" + super.getCantidad() + ", tipoAportacion="
                + super.getTipoAportacion() + ", fechaIngreso=" + super.getFechaIngreso() + ", nombreAportador="
                + super.getNombreAportador() + ", idAportador=" + super.getIdAportador() + ", idOng=" + super.getIdOng()
                + ", origen de la aportación=" + this.origenAportacion + '}';
    }

}

    //------------------------------------ CÓDIGO DESCARTADO ------------------------------------//

    /*/**
     * Método que devuelve el origen de la aportación en forma de String
     * @return Devuelve un String que corresponde al origen de la aportación
    */
    /*public String getOrigenAportacionString() {
        String pTipo = null;
       if(this.getOrigenAportacion().equals(OrigenAportacion.INGRESOEXTRA)) {
            pTipo = "INGRESO EXTRAORDINARIO";
        }
        else if(this.getOrigenAportacion().equals(OrigenAportacion.PARTICULAR)) {
            pTipo = "APORTACIÓN PUNTUAL DE PARTICULARES";
        }
        else if(this.getOrigenAportacion().equals(OrigenAportacion.EMPRESA)) {
            pTipo = "APORTACIÓN PUNTUAL DE EMPRESA";
        }
        else if(this.getOrigenAportacion().equals(OrigenAportacion.HERENCIASLEGADOS)) {
            pTipo = "APORTACIÓN PUNTUAL DE HERENCIAS Y LEGADOS";
        }
        else if(this.getOrigenAportacion().equals(OrigenAportacion.INSTITUCIONES)) {
            pTipo = "APORTACIÓN PUNTUAL DE INSTITUCIONES";
        }
        else if(this.getOrigenAportacion().equals(OrigenAportacion.CUOTASOCIO)) {
            pTipo = "CUOTA DE SOCIOS";
        }
        return pTipo;
    }
*/


