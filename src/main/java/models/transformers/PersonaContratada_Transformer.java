package models.transformers;

import java.time.LocalDate;

public class PersonaContratada_Transformer extends Equipo_Transformer {

    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//

    private float sueldo;

    //--------------------------------- RELACIONES ENTRE CLASES ---------------------------------//
    //NO HAY

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//

    /**
     * Creamos constructor default
     * @param sueldo nómina/salario del objeto de persona contratada
     * @param nombre nombre de la persona contratada
     * @param apellido1 primer apellido
     * @param apellido2 segundo apellido
     * (ambos parametros: apellidos de la persona contratada)
     * @param telefono número de contacto de la persona contratada
     * @param user usuario de incio de sesión
     * @param fechaAltaMiembroEquipo fecha de inicio en equipo
     * @param password contraseña del usuario del inicio de sesión
     * @param rol atributo proviniente de la clase Rol. Rol de la persona contratada,
     * que puede tratarse de USUARIO o ADMINISTRADOR
     */
    public PersonaContratada_Transformer(String nombre, String apellido1, String apellido2, String telefono,
                                         String user, String password, LocalDate fechaAltaMiembroEquipo, LocalDate fechaBajaMiembroEquipo, Rol rol, Long idOng, float sueldo) {
        super(nombre, apellido1, apellido2, telefono, user, password, fechaAltaMiembroEquipo, fechaBajaMiembroEquipo, rol, idOng);
        this.sueldo = sueldo;
    }



    //------------------------------------GETTERS Y SETTERS---------------------------------

    public float getSueldo() { return sueldo; }

    public void setSueldo(float sueldo) {this.sueldo = sueldo;  }


    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//

    @Override
    public String toString() {
        return "PersonaContratada{" + "nombre=" + super.getNombre() + ", apellido1=" + super.getApellido1() + ", apellido2=" + super.getApellido2() + ", id=" + super.getId() +
                ", telefono=" + super.getTelefono() + ", user=" + super.getUser() + ", password=" + super.getPassword() + ", fechaAltaMiembroEquipo=" +
                super.getFechaAltaMiembroEquipo() + ", fechaBajaMiembroEquipo=" + super.getFechaBajaMiembroEquipo() + ", rol=" + super.getRol() + "sueldo=" + sueldo + '}';
    }

}
