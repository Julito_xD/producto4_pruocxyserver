package models.transformers;

public class SublineaAccion_Transformer {
    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//
    private Long idSublinea;
    private String concepto;

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//

    /**
     * Constructor de SublineaAccion vacio por defecto
     */
    public SublineaAccion_Transformer() {
    }

    /**
     * Constructor de SublineaAccion
     * @param idSublinea
     * @param concepto
     */
    public SublineaAccion_Transformer(Long idSublinea, String concepto) {
        this.idSublinea = idSublinea;
        this.concepto = concepto;
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//

    public Long getIdSublinea() {
        return idSublinea;
    }

    public void setIdSublinea(Long idSublinea) {
        this.idSublinea = idSublinea;
    }
    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//
    @Override
    public String toString() {
        return "SublineaAccion{" +
                "idSublinea=" + idSublinea +
                ", concepto=" + concepto +
                '}';
    }
}