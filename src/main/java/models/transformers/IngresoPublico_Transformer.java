package models.transformers;

import java.time.LocalDate;

public class IngresoPublico_Transformer extends Ingreso_Transformer {
    /**
     * Definición del tipo enumerado TipoAportador que definirá el tipo de aportador del ingreso público
     * Tipos de aportadores que realizan ingresos públicos a la ONG
     */
    public enum TipoAportador {ADMIN_ESTATAL, ADMIN_AUTONOMICA, ADMIN_LOCAL, SUBVENCION_UE};

    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//
    
    private String tipoAportador;

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//

    /**
    * Sobrecarga de métodos constructores - Método constructor por defecto
    */
    public IngresoPublico_Transformer() {
        
    }
    
    /**
     * Método constructor para los Ingresos Públicos para trabajar con XML
     * @param idIngreso Este parámetro es de tipo int y define el identificador del ingreso público
     * @param cantidad Este parámetro es de tipo float y define la cantidad del ingreso público en forma decimal
     * @param tipoAportacion Este parámetro es del tipo enumerado TipoAportacion y define el tipo de aportación del ingreso público
     * @param fechaIngreso Este parámetro es de tipo LocalDate y define la fecha en que se realizó el ingreso público
     * @param nombreAportador Este parámetro es de tipo String y define el nombre del aportador
     * @param idAportador Este parámetro es de tipo String y define el ID del aportador
     * @param tipoAportador Este parámetro es del tipo enumerado TipoAportador y define el tipo de aportador que realiza un ingreso público a la ONG
     */
    public IngresoPublico_Transformer(int idIngreso, float cantidad, String tipoAportacion, LocalDate fechaIngreso,
                                      String nombreAportador, String idAportador, Long idOng, String tipoAportador) {
        super(idIngreso, cantidad, tipoAportacion, fechaIngreso, nombreAportador, idAportador, idOng);
        //super.getIdIngreso();
        this.tipoAportador = tipoAportador;
    }

    /**
     * Método constructor para los Ingresos Públicos para trabajar con MySQL
     * @param cantidad Este parámetro es de tipo float y define la cantidad del ingreso público en forma decimal
     * @param tipoAportacion Este parámetro es del tipo enumerado TipoAportacion y define el tipo de aportación del ingreso público
     * @param fechaIngreso Este parámetro es de tipo LocalDate y define la fecha en que se realizó el ingreso público
     * @param nombreAportador Este parámetro es de tipo String y define el nombre del aportador
     * @param idAportador Este parámetro es de tipo String y define el ID del aportador
     * @param idOng Este parámetro es de tipo Long y define el ID de la ONG (sede o delegación)
     * @param tipoAportador Este parámetro es del tipo enumerado TipoAportador y define el tipo de aportador que realiza un ingreso público a la ONG
     */
    public IngresoPublico_Transformer(float cantidad, String tipoAportacion, LocalDate fechaIngreso,
                                      String nombreAportador, String idAportador, Long idOng, String tipoAportador) {
        super(cantidad, tipoAportacion, fechaIngreso, nombreAportador, idAportador, idOng);
        this.tipoAportador = tipoAportador;
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//

    public String getTipoAportador() {
        return tipoAportador;
    }

    public void setTipoAportador(String tipoAportador) {
        this.tipoAportador = tipoAportador;
    }

    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//


    //---------------------------------------- TO STRING ---------------------------------------//
    /**
     * Método que permite mostrar la información completa de un objeto de la clase Ingreso Público
     * @return Devuelve un String con todos los datos del ingreso público
     */
    public String toString() {
        return "Ingreso Público{" + "idIngreso=" + super.getIdIngreso() + ", cantidad=" + super.getCantidad() + ", tipoAportacion="
                + super.getTipoAportacion() + ", fechaIngreso=" + super.getFechaIngreso() + ", nombreAportador="
                + super.getNombreAportador() + ", idAportador=" + super.getIdAportador() + ", idOng=" + super.getIdOng()
                + ", tipo de aportador=" + this.tipoAportador + '}';
    }

}

    //------------------------------------ CÓDIGO DESCARTADO ------------------------------------//
    /*/**
     * Método que devuelve el tipo de Aportador en forma de String
     * @return Devuelve un String que corresponde al tipo de aportador del ingreso público
     */
    /*public String getTipoAportadorString() {
        String pTipo = null;
        if(this.getTipoAportador().equals(TipoAportador.ADMIN_ESTATAL)) {
            pTipo = "ADMINISTRACIÓN ESTATAL";
        }
        else if(this.getTipoAportador().equals(TipoAportador.ADMIN_AUTONOMICA)) {
            pTipo = "ADMINISTRACIÓN AUTONÓMICA";
        }
        else if(this.getTipoAportador().equals(TipoAportador.ADMIN_LOCAL)) {
            pTipo = "ADMINISTRACIÓN LOCAL";
        }
        else if(this.getTipoAportador().equals(TipoAportador.SUBVENCION_UE)) {
            pTipo = "SUBVENCIÓN UNIÓN EUROPEA";
        }
        return pTipo;
    }*/

