package models.transformers;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;


import javax.persistence.*;

@Entity(name = "socio")
@Table

public class Socio_Transformer implements Serializable {


    private static final long serialVersionUID = 1L;
    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//
    @Id
    @Column(name = "idSocio")
    //@GeneratedValue(generator = "incrementor")
    private long id;
    @Column (name = "nombre")
    private String nombre;
    @Column (name = "apellido1")
    private String apellido1;
    @Column (name = "apellido2")
    private String apellido2;
    @Column (name = "NIF")
    private String dni;
    @Column (name = "cuentaBancaria")
    private String cuentaBancaria;
    @Column (name = "fechaAlta")
    private LocalDate fechaAltaSocio;
    @Column (name = "fechaBaja")
    private LocalDate fechaBajaSocio;
    @Column (name = "tipoCuota")
    private String tipoCuota;





    //--------------------------------- RELACIONES ENTRE CLASES ---------------------------------//

   // private IngresoPrivado_Transformer ingreso;
      @JoinColumn(name = "bbdd_ong", referencedColumnName = "idONG")
      private Long ong;

    //private ArrayList<Proyecto_Transformer> proyectosAsignados;

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//

    /**
     * Constructor vacío
     */
    public Socio_Transformer() {
    }

    /**
     * El presente método constructor instancia un SOCIO sin ingresos
     * @param nombre
     * @param apellido1
     * @param apellido2
     * @param id
     */
    public Socio_Transformer(long id, String dni, String nombre, String apellido1, String apellido2, String cuentaBancaria,
                             LocalDate fechaAltaSocio) {

        this.dni = dni;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.id = id;
        this.cuentaBancaria = cuentaBancaria;
        this.fechaAltaSocio = fechaAltaSocio;

    }

    /**
     * El presente método constructor instancia un SOCIO y un Ingreso nuevo dentro del SOCIO
     * @param nombre
     * @param apellido1
     * @param apellido2
     * @param id
     * @param delegacion
     */
    public Socio_Transformer(long id, String dni, String nombre, String apellido1, String apellido2, LocalDate fechaAltaSocio, OrganizacionONG_Transformer delegacion) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.id = id;
        //this.ingreso = new IngresoPrivado();
        this.fechaAltaSocio = fechaAltaSocio;
        //metodo para agregar está clase Socio en la SedeCentral
    }

    public Socio_Transformer(long id, String dni, String nombre, String apellido1, String apellido2, String cuentaBancaria) {

        this.dni = dni;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.id = id;
        this.cuentaBancaria = cuentaBancaria;

        //metodo para agregar está clase Socio en la SedeCentral
    }

    /**
     * ESTE EL BUENO :D
     * @param nombre
     * @param apellido1
     * @param apellido2
     * @param dni
     * @param cuentaBancaria
     * @param fechaAltaSocio
     * @param fechaBajaSocio
     * @param tipoCuota
     * @param ong
     */
    public Socio_Transformer(String nombre, String apellido1, String apellido2, String dni, String cuentaBancaria, LocalDate fechaAltaSocio, LocalDate fechaBajaSocio, String tipoCuota, Long ong) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.dni = dni;
        this.cuentaBancaria = cuentaBancaria;
        this.fechaAltaSocio = fechaAltaSocio;
        this.fechaBajaSocio = fechaBajaSocio;
        this.tipoCuota = tipoCuota;
        this.ong = ong;
    }

    public Socio_Transformer(Long id, String nombre, String apellido1, String apellido2, String dni, String cuentaBancaria, LocalDate fechaAltaSocio, LocalDate fechaBajaSocio, String tipoCuota, Long ong) {
        this.id = id;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.dni = dni;
        this.cuentaBancaria = cuentaBancaria;
        this.fechaAltaSocio = fechaAltaSocio;
        this.fechaBajaSocio = fechaBajaSocio;
        this.tipoCuota = tipoCuota;
        this.ong = ong;
    }



    //------------------------------------ GETTERS Y SETTERS ------------------------------------//

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }
    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }
    public String getCuentaBancaria() {
        return cuentaBancaria;
    }

    public void setCuentaBancaria(String cuentaBancaria) { this.cuentaBancaria = cuentaBancaria;}
    public LocalDate getFechaAltaSocio() {
        return fechaAltaSocio;
    }

    public void setFechaAltaSocio(LocalDate fechaAltaSocio) {
        this.fechaAltaSocio = fechaAltaSocio;
    }
    public LocalDate getFechaBajaSocio() {
        return fechaBajaSocio;
    }

    public void setFechaBajaSocio(LocalDate fechaBajaSocio) {
        this.fechaBajaSocio = fechaBajaSocio;
    }
    public String getTipoCuota() {
        return tipoCuota;
    }

    public void setTipoCuota(String tipoCuota) {
        this.tipoCuota = tipoCuota;
    }

    public Long getOng() {
        return ong;
    }

    public void setOng(Long ong) {
        this.ong = ong;
    }

    @Override
    public String toString() {
        return "Socio{" +
                "id=" + id +
                ", dni='" + dni + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", cuentaBancaria='" + cuentaBancaria + '\'' +
                ", tipoCuota=" + tipoCuota +
                ", fechaAltaSocio=" + fechaAltaSocio +
                ", fechaBajaSocio=" + fechaBajaSocio +
                ", ong=" + ong +
                '}';
    }


    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//

    /**
     * Devuelve el tipo de cuota del socio.
     * @return
     */
    /*public String tipoCuota(){

        String pTipoCuota = null;

        if (this.tipoCuota == tipoCuota.CUOTAMENSUAL)
            pTipoCuota = "MENSUAL";

        else if (this.tipoCuota == tipoCuota.CUOTATRIMESTRAL)
            pTipoCuota = "TRIMESTRAL";

        else if (this.tipoCuota == tipoCuota.CUOTAANUAL)
            pTipoCuota = "ANUAL";

        return pTipoCuota;
    }*/
    //setDarBaja
    //setAgregarIngreso
    //setModificarIngreso
    //getMostrarIngresos
    //setAsignarProyecto
    //getMostrarListaProyectosAsignados

/*
    void FechaAlta (){

        Scanner sc = new Scanner(System.in);

        System.out.println("Escriba la fecha de alta");
        String entrada = sc.next(); // Entrada recogida como sea (scanner)
        DateFormat format = new SimpleDateFormat("DD/MM/YYYY"); // Creamos un formato de fecha
        Date fecha = format.parse(entrada); // Creamos un date con la entrada en el formato especificado
        System.out.println(fecha);

    }*/

}


