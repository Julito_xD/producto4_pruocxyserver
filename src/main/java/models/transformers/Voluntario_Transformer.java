package models.transformers;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity(name = "voluntario")
@Table
public class Voluntario_Transformer extends Equipo_Transformer {

    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//
    @Column(name = "horasPorSemana")
    private float horasPorSemana;

    //--------------------------------- RELACIONES ENTRE CLASES ---------------------------------//
    //NO HAY

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//
    public Voluntario_Transformer(){

    }


    /**
     * Creamos constructor default
     * @param horasPorSemana horas trabajadas a la semana
     * @param nombre nombre del voluntario
     * @param apellido1 primer apellido
     * @param apellido2 segundo apelllido
     * @param telefono número de contacto del voluntario
     * @param user usuario de incio de sesión
     * @param fechaAltaMiembroEquipo fecha de inicio en equipo
     * @param password contraseña del usuario del inicio de sesión
     * @param rol atributo proviniente de la clase Rol. Rol del objeto voluntario,
     * que puede tratarse de USUARIO o ADMINISTRADOR
     */
    public Voluntario_Transformer(String nombre, String apellido1, String apellido2, String telefono, String user, String password,
                                  LocalDate fechaAltaMiembroEquipo, LocalDate fechaBajaMiembroEquipo, Rol rol, Long idOng, float horasPorSemana) {
        super(nombre, apellido1, apellido2, telefono, user, password, fechaAltaMiembroEquipo, fechaBajaMiembroEquipo, rol, idOng);
        this.horasPorSemana = horasPorSemana;
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//

    public float getHorasPorSemana() {return horasPorSemana; }

    public void setHorasPorSemana(float HorasPorSemana){ this.horasPorSemana = horasPorSemana; }

    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//

    @Override
    public String toString() {
        return "Voluntario{" +  "nombre=" + super.getNombre() + ", apellido1=" + super.getApellido1() + ", apellido2=" + super.getApellido2() + ", id=" + super.getId() +
                ", telefono=" + super.getTelefono() + ", user=" + super.getUser() + ", password=" + super.getPassword() + ", fechaAltaMiembroEquipo=" +
                super.getFechaAltaMiembroEquipo() + ", fechaBajaMiembroEquipo=" + super.getFechaBajaMiembroEquipo() + ", rol=" + super.getRol() +
                "horasPorSemana=" + horasPorSemana + '}';
    }

}
