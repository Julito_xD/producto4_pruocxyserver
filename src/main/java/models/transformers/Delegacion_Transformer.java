package models.transformers;

public class Delegacion_Transformer extends OrganizacionONG_Transformer {
    //--------------------------------------- CONSTRUCTOR ---------------------------------------//
    /**
     * Constructor vacio de la clase Delegacion
     */
    public Delegacion_Transformer() {
    }

    /**
     * Constructor de la clase Delegacion para los CRUD XML
     * @param nombreSede
     * @param idONG
     * @param ubicacion
     */
    public Delegacion_Transformer(int idONG, String nombreSede, String ubicacion) {
        super(idONG, nombreSede, ubicacion);

    }

    /**
     * Constructor de la clase SedeCentral para los CRUD MySQL
     * @param nombreSede
     * @param ubicacion
     */
    public Delegacion_Transformer(String nombreSede, String ubicacion) {
        super(nombreSede, ubicacion);
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//
    //De momento no es necesario
    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//
    //De momento no es necesario
    //---------------------------------------- TO STRING ---------------------------------------//
    //De momento no es necesario
}