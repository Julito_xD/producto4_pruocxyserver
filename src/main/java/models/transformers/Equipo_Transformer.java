package models.transformers;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

@Entity(name = "equipo")
@Table
public abstract class Equipo_Transformer implements Serializable {

    private static final long serialVersionUID = 2L;

    public enum Rol { USUARIO, ADMINISTRADOR}
    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idEquipo")
    private Long id;
    //private static final AtomicLong count = new AtomicLong(0L);
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellido1")
    private String apellido1;
    @Column(name = "apellido2")
    private String apellido2;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "usuario")
    private String user;
    @Column(name = "password")
    private String password;
    //@Temporal(TemporalType.DATE) ----> @Temporal sólo se usa en DATE, no en LOCALDATE
    @Column(name = "altaMiembroEquipo")
    private LocalDate fechaAltaMiembroEquipo;
    //@Temporal(TemporalType.DATE)
    @Column(name = "bajaMiembroEquipo")
    private LocalDate fechaBajaMiembroEquipo;
    @Column(name = "rol", columnDefinition = "enum('USUARIO', 'ADMINISTRADOR')")
    @Enumerated(EnumType.STRING)
    Rol rol;
    @Column(name = "idONG")
    private Long idOng;

    //--------------------------------- RELACIONES ENTRE CLASES ---------------------------------//
    //private Delegacion_Transformer delegacionTransformer;

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//
    /**
    * Sobrecarga de métodos constructores - Método constructor por defecto
    */

    public Equipo_Transformer() {

    }

    //With ID constructor
    public Equipo_Transformer(String nombre, String apellido1, String apellido2, Long id, String telefono, String user, String password, LocalDate fechaAltaMiembroEquipo, LocalDate fechaBajaMiembroEquipo, Rol rol, Long idOng) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.id = id;
        this.telefono = telefono;
        this.user = user;
        this.password = password;
        this.fechaAltaMiembroEquipo = fechaAltaMiembroEquipo;
        this.fechaBajaMiembroEquipo = fechaBajaMiembroEquipo;
        this.rol = rol;
        this.idOng = idOng;
    }

    //No ID constructor
    /**
     * Creamos constructor default
     * @param nombre nombre
     * @param apellido1 primer apellido
     * @param apellido2 segundo apellido
     * (ambos parametros: apellidos)
     * @param telefono número de contacto
     * @param user usuario de incio de sesión
     * @param password contraseña del usuario del inicio de sesión
     * @param fechaAltaMiembroEquipo fecha de inicio en equipo
     * @param fechaBajaMiembroEquipo fecha de baja en equipo
     * @param rol atributo proviniente de la clase Rol. Rol
     * que puede tratarse de USUARIO o ADMINISTRADOR
     */
    public Equipo_Transformer(String nombre, String apellido1, String apellido2, String telefono, String user, String password, LocalDate fechaAltaMiembroEquipo,
                              LocalDate fechaBajaMiembroEquipo, Rol rol, Long idOng) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        //this.id = count.getAndIncrement();
        this.telefono = telefono;
        this.user = user;
        this.password = password;
        this.fechaAltaMiembroEquipo = fechaAltaMiembroEquipo;
        this.fechaBajaMiembroEquipo = fechaBajaMiembroEquipo;
        this.rol = rol;
        this.idOng = idOng;
    }
    
    //------------------------------------ GETTERS Y SETTERS ------------------------------------//

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long idEquipo) {
        this.id = idEquipo;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public LocalDate getFechaAltaMiembroEquipo() {
        return fechaAltaMiembroEquipo;
    }

    public LocalDate getFechaBajaMiembroEquipo() {
        return fechaBajaMiembroEquipo;
    }

    public Rol getRol() {
        return rol;
    }

    public Long getIdOng() {
        return idOng;
    }

    public void setIdOng(Long idOng) {
        this.idOng = idOng;
    }

    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//
    //setAsignarMiembroEquipoAProyecto
    //setAsignarMiembroEquipoADelegacion 
    
    @Override
    public String toString() {
        return "Equipo{" + "nombre=" + nombre + ", apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", id=" + id + ", telefono=" + telefono + ", user=" + user + ", password=" + password + ", " +
                "fechaAltaMiembroEquipo=" + fechaAltaMiembroEquipo + ", fechaBajaMiembroEquipo=" + fechaBajaMiembroEquipo + ", rol=" + rol /*+ ", idOng=" + delegacionTransformer.getIdONG()*/ + '}';
    }
}
