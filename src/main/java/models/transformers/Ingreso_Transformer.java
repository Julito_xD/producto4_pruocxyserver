package models.transformers;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicReference;

public abstract class Ingreso_Transformer {

    /**
     * Definición del tipo enumerado TipoAportacion que definirá el tipo de aportación realizada a la ONG
     * Tipos de aportación para los Ingresos
     */
    public enum TipoAportacion {AP_PUNTUAL, AP_RECURRENTE,}


    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//
    
    private Long idIngreso;
    private static final AtomicReference<Long> id = new AtomicReference<>(0L);
    private float cantidad;
    private String tipoAportacion;
    private LocalDate fechaIngreso;
    private String nombreAportador;
    private String idAportador;
    //--------------------------------- RELACIONES ENTRE CLASES ----------------------------------//
    private Long idOng;

    //--------------------------------------- CONSTRUCTOR ----------------------------------------//

    /**
    * Sobrecarga de métodos constructores - Método constructor por defecto
    */
    public Ingreso_Transformer() {
    }
    
    /**
     * Método constructor para los Ingresos para trabajar con XML
     * @param idIngreso Este parámetro es de tipo int y define el identificador del ingreso
     * @param cantidad Este parámetro es de tipo float y define la cantidad del ingreso en forma decimal
     * @param tipoAportacion Este parámetro es del tipo enumerado TipoAportacion y define el tipo de aportación del ingreso
     * @param fechaIngreso Este parámetro es de tipo LocalDate y define la fecha en que se realizó el ingreso
     * @param nombreAportador Este parámetro es de tipo String y define el nombre del aportador
     * @param idAportador Este parámetro es de tipo String y define el ID del aportador
     */
    public Ingreso_Transformer(int idIngreso, float cantidad, String tipoAportacion, LocalDate fechaIngreso, String nombreAportador, String idAportador, Long idOng) {
        this.idIngreso = Long.valueOf(idIngreso);
        //this.idIngreso = id.accumulateAndGet(1L, (x, y) -> x+y);
        this.cantidad = cantidad;
        this.tipoAportacion = tipoAportacion;
        this.fechaIngreso = fechaIngreso;
        this.nombreAportador = nombreAportador;
        this.idAportador = idAportador;
    }

    /**
     * Método constructor para los Ingresos para trabajar con MySQL
     * @param cantidad Este parámetro es de tipo float y define la cantidad del ingreso en forma decimal
     * @param tipoAportacion Este parámetro es del tipo enumerado TipoAportacion y define el tipo de aportación del ingreso
     * @param fechaIngreso Este parámetro es de tipo LocalDate y define la fecha en que se realizó el ingreso
     * @param nombreAportador Este parámetro es de tipo String y define el nombre del aportador
     * @param idAportador Este parámetro es de tipo String y define el ID del aportador
     * @param idOng Este parámetro es de tipo Long y define el ID de la ONG (sede o delegación)
     */
    public Ingreso_Transformer(float cantidad, String tipoAportacion, LocalDate fechaIngreso, String nombreAportador,
                               String idAportador, Long idOng) {
        this.cantidad = cantidad;
        this.tipoAportacion = tipoAportacion;
        this.fechaIngreso = fechaIngreso;
        this.nombreAportador = nombreAportador;
        this.idAportador = idAportador;
        this.idOng = Long.valueOf(idOng);
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//

    public Long getIdIngreso() {
        return idIngreso;
    }

    public void setIdIngreso(Long idIngreso) {
        this.idIngreso = idIngreso;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getTipoAportacion() {
        return tipoAportacion;
    }

    public void setTipoAportacion(String tipoAportacion) {
        this.tipoAportacion = tipoAportacion;
    }
    
    public String getNombreAportador() {
        return nombreAportador;
    }

    public void setNombreAportador(String nombreAportador) {
        this.nombreAportador = nombreAportador;
    }

    public String getIdAportador() {
        return idAportador;
    }

    public void setIdAportador(String idAportador) {
        this.idAportador = idAportador;
    }

    public Long getIdOng() {
        return idOng;
    }

    public void setIdOng(Long idOng) {
        this.idOng = idOng;
    }

    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//
    //setAgregarIngresoONG();

    //---------------------------------------- TO STRING ---------------------------------------//
    //De momento no es necesario

    
}
