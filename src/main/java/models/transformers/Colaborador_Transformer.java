package models.transformers;

import java.time.LocalDate;

public class Colaborador_Transformer extends Equipo_Transformer {

    //------------------------------------ ATRIBUTOS DE CLASE ------------------------------------//

    private String tipoColaboracion;

    //--------------------------------- RELACIONES ENTRE CLASES ---------------------------------//
    //NO HAY

    //--------------------------------------- CONSTRUCTOR ---------------------------------------//

    /**
     * Creamos constructor default
     * @param tipoColaboracion residencia/base guardada como String del colaborador
     * @param nombre nombre del colaborador
     * @param apellido1 primer apellido
     * @param apellido2 segundo apellido
     * (ambos parametros: apellidos del colaborador)
     * @param telefono n&uacute;mero de contacto del colaborador
     * @param user usuario de incio de sesion
     * @param password contrase&ntilde;a del usuario del inicio de sesion
     * @param fechaAltaMiembroEquipo fecha de inicio en equipo
     * @param fechaBajaMiembroEquipo fecha de baja en equipo
     * @param rol atributo proviniente de la clase Rol. Rol del objeto colaborador,
     * que puede tratarse de USUARIO o ADMINISTRADOR
     * @param tipoColaboracion el tipo de participacion del colaborador, se trata de un String
     */
    public Colaborador_Transformer(String nombre, String apellido1, String apellido2, Long id, String telefono,
                                   String user, String password, LocalDate fechaAltaMiembroEquipo, LocalDate fechaBajaMiembroEquipo, Rol rol, Long idOng, String tipoColaboracion) {
        super(nombre, apellido1, apellido2, id, telefono, user, password, fechaAltaMiembroEquipo, fechaBajaMiembroEquipo, rol, idOng);
        this.tipoColaboracion = tipoColaboracion;
    }


    //No ID constructor
    /**
     * Creamos constructor default
     * @param tipoColaboracion residencia/base guardada como String del colaborador
     * @param nombre nombre del colaborador
     * @param apellido1 primer apellido
     * @param apellido2 segundo apellido
     * (ambos parametros: apellidos del colaborador)
     * @param telefono n&uacute;mero de contacto del colaborador
     * @param user usuario de incio de sesion
     * @param password contrase&ntilde;a del usuario del inicio de sesion
     * @param fechaAltaMiembroEquipo fecha de inicio en equipo
     * @param fechaBajaMiembroEquipo fecha de baja en equipo
     * @param rol atributo proviniente de la clase Rol. Rol del objeto colaborador,
     * que puede tratarse de USUARIO o ADMINISTRADOR
     * @param tipoColaboracion el tipo de participacion del colaborador, se trata de un String
     */
    public Colaborador_Transformer(String nombre, String apellido1, String apellido2, String telefono,
                                   String user, String password, LocalDate fechaAltaMiembroEquipo, LocalDate fechaBajaMiembroEquipo, Rol rol, Long idOng, String tipoColaboracion) {
        super(nombre, apellido1, apellido2, telefono, user, password, fechaAltaMiembroEquipo, fechaBajaMiembroEquipo, rol, idOng);
        this.tipoColaboracion = tipoColaboracion;
    }

    //------------------------------------ GETTERS Y SETTERS ------------------------------------//

    public String getTipoColaboracion() {
        return tipoColaboracion;
    }

    public void setTipoColaboracion(String tipoColaboracion) {this.tipoColaboracion = tipoColaboracion; }


    //------------------------------------ MÉTODOS DE CLASE ------------------------------------//

    @Override
    public String toString() {
        return "Colaborador{" + "nombre=" + super.getNombre() + ", apellido1=" + super.getApellido1() + ", apellido2=" + super.getApellido2() + ", id=" + super.getId() +
                ", telefono=" + super.getTelefono() + ", user=" + super.getUser() + ", password=" + super.getPassword() + ", fechaAltaMiembroEquipo=" +
                super.getFechaAltaMiembroEquipo() + ", fechaBajaMiembroEquipo=" + super.getFechaBajaMiembroEquipo() + ", rol=" + super.getRol() + ", idOng=" + super.getIdOng() +" tipoColaboracion=" +
                tipoColaboracion + '}';
    }
}
