package dao;

//------------------COMENTAR Y DESCOMENTAR SEGUN NECESIDAD ------------------//
import jakarta.xml.bind.JAXBException;
import java.sql.SQLException;
import java.util.List;


public interface DAO_Generico<T, K> {
    public void insert  (T a) throws JAXBException, SQLException;
    public void update  (T a) throws JAXBException, SQLException;
    public void delete  (T a) throws JAXBException, SQLException;
    public void deleID  (K id) throws SQLException, JAXBException;
    public T    getOne  (K id) throws SQLException;
    public List<T> getAll() throws SQLException;
}

