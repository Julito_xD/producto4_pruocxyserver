package dao;
import models.transformers.SublineaAccion_Transformer;

public interface SublineaAccionDAO extends DAO_Generico<SublineaAccion_Transformer, Long> {
}