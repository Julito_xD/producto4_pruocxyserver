package dao;
import models.transformers.IngresoPrivado_Transformer;

public interface IngresoPrivadoDAO extends DAO_Generico<IngresoPrivado_Transformer, Long> {
}
