package dao;

import models.models.*;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Clase Factory para instanciar los DAO
 * @author Laura Zafra
 */
public abstract class DAO_Factory {
    //TIPOS DE FACTORIA
    public static final int XML = 1;
    public static final int MYSQL = 2;

    //---------- DEFINICIÓN DE MÉTODOS QUE IMPLEMENTAN LOS DAO PARA MySQL ---------- //
    public abstract MySQLcolaboradorIMPL getColaboradorIMPL() throws SQLException;
    public abstract MySQLdelegacionIMPL getDelegacionIMPL() throws SQLException;
    public abstract MySQLingresoPrivadoIMPL getIngresoPrivadoIMPL() throws SQLException;
    public abstract MySQLingresoPublicoIMPL getIngresoPublicoIMPL() throws SQLException;
    public abstract MySQLlineaAccionIMPL getLineaAccionIMPL() throws SQLException;
    public abstract MySQLPersonaContratadaIMPL getPersonaContratadaIMPL() throws SQLException;
    public abstract MySQLProyectoIMPL getProyectoIMPL() throws SQLException;
    public abstract MySQLsedeCentralIMPL getSedeCentralIMPL() throws SQLException;
    public abstract MySQLsocioIMPL getSocioIMPL() throws SQLException;
    public abstract MySQLsublineaAccionIMPL getSublineaAccionIMPL() throws SQLException;
    public abstract MySQLVoluntarioIMPL getVoluntarioIMPL() throws SQLException;
    public abstract MySQLVoluntarioInternacionalIMPL getVoluntarioInternacionalIMPL() throws SQLException;
    
    public static DAO_Factory getFactoryIMPL(int tipoFactory) {
        switch(tipoFactory) {
            case XML:
                //return new XMLFactoryIMPL();
            case MYSQL:
                return new MySQLFactoryIMPL();
            default:
                return null;
        }
    }

    public abstract void setConnection(Connection conn);
}
