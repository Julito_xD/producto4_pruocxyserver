package dao;
import models.transformers.VoluntarioInternacional_Transformer;

public interface VoluntarioInternacionalDAO extends DAO_Generico<VoluntarioInternacional_Transformer, Long> {
}
