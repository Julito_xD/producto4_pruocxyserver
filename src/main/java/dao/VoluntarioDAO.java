package dao;
import models.transformers.Voluntario_Transformer;

public interface VoluntarioDAO extends DAO_Generico<Voluntario_Transformer, Long> {
}
